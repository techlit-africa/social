class Typing::LessonsController < Typing::BaseController
  def index
    submissions_by_exercise_id =
      @current_user.submissions.where('complete or accurate or fast').group_by(&:exercise_id)

    lesson_available = true
    lessons = Typing::Lesson.for_home_page.map do |lesson|
      lesson_complete = true
      lesson_accurate = true
      lesson_fast = true

      exercise_available = lesson_available
      exercises = lesson[:exercises].map do |exercise|
        submissions = submissions_by_exercise_id[exercise[:id]]

        lesson_complete &&= exercise_complete = !!submissions&.any? { _1[:complete] }
        lesson_accurate &&= exercise_accurate = !!submissions&.any? { _1[:accurate] }
        lesson_fast &&= exercise_fast = !!submissions&.any? { _1[:fast] }

        vm = {
          **exercise,
          available: exercise_available,
          complete: exercise_complete,
          accurate: exercise_accurate,
          fast: exercise_fast,
        }
        exercise_available &&= exercise_complete
        vm
      end

      vm = {
        **lesson,
        exercises: exercises,
        available: lesson_available,
        complete: lesson_complete,
        accurate: lesson_accurate,
        fast: lesson_fast,
      }
      lesson_available &&= lesson_complete
      vm
    end

    render json: lessons
  end
end
