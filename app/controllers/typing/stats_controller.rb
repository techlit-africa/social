class Typing::StatsController < Typing::BaseController
  def index
    @stats = Typing::Stats.new
    render json: @stats.global
  end
end
