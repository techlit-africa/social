class Typing::ExercisesController < Typing::BaseController
  def show
    @exercise = Typing::Exercise
      .joins(:lesson)
      .select(
        'exercises.*',
        'lessons.title as lesson_title',
        'lessons.position as lesson_position',
      )
      .find(params[:id])

    render json: @exercise.attributes
  end
end
