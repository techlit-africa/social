class SessionsController < ApplicationController
  def create
    unless @user = User.with_klass_name.where(username: params[:username]).first
      render json: {error: "Wrong username. Is it typed correctly?"}, status: :not_found
    else
      if (@user.password_digest.nil? && params[:password] == @user.username) || @user.authenticate(params[:password])
        render json: {token: JsonWebToken.encode(user_id: @user.id), **@user.attributes}
      else
        render json: {error: 'Wrong password. Is this your account?'}, status: :not_found
      end
    end
  end
end
