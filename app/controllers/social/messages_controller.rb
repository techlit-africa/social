class Social::MessagesController < Social::BaseController
  before_action :set_message, only: %i[update destroy]

  def index
    json = Social::Message
      .joins(user: :klass)
      .select(
        "messages.*",
        "klasses.name as klass",
        "users.username",
        "users.display_name",
        "users.tagline",
      )
      .order(created_at: :desc)
      .limit(100)
      .map(&:attributes)
      .reverse

    render json: json
  end

  def create
    @message = Social::Message.new(user: @current_user, **message_params)
    if @message.save
      render_success
    else
      render_error
    end
  end

  def update
    if @message.update(message_params)
      render_success
    else
      render_error
    end
  end

  def destroy
    if @message.destroy
      render_success
    else
      render_error
    end
  end

  private

  def set_message
    @message = @current_user.messages.find(params[:id])
  end

  def message_params
    params.permit(:content)
  end

  def render_success
    view =
      @message.attributes.merge \
        "action" => action_name,
        **@current_user.attributes.slice("username", "display_name", "tagline")

    broadcast "MessagesChannel", view
    render json: view
  end

  def render_error
    error = @message.errors.full_messages.to_sentence
    render json: {error: error}, status: :unprocessable_entity
  end
end
