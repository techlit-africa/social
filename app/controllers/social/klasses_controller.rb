class Social::KlassesController < Social::BaseController
  def index
    render json: Klass.all
  end
end
