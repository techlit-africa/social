class Social::ProjectsController < Social::BaseController
  skip_before_action :authorize_user, only: %i[download]
  before_action :set_project, except: %i[index download create]

  def index
    @projects =
      Social::Project
        .where("content_type like 'image%' or content_type like 'video%' or content_type like 'audio%'")
        .order(updated_at: :desc)
        .limit(100)

    render json: @projects
  end

  def show
    render_success
  end

  def download
    root = Rails.application.config.project_storage_root
    filename = File.basename(request.original_url)
    path = File.join(root, params[:username], filename)

    File.open(path, "r") do |f|
      send_file f
    end
  end

  def create
    unless params[:file]
      error = "Upload a file to share your project!"
      render json: {error: error}, status: :unprocessable_entity
      return
    end

    filename = params[:file]&.original_filename || ''
    extension = File.extname(filename)[1..]
    storage_path =
      Social::Project.storage_path_for(@current_user, params[:name], extension)

    @project = Social::Project.new(
      user: @current_user,
      storage_path: storage_path,
      size_in_bytes: params[:file]&.tempfile&.size.to_i,
      content_type: params[:file]&.content_type,
      **params.permit(:name, :description),
    )
    if @project.valid? && @project.save_with_file(params[:file])
      render_success
    else
      render_error
    end
  end

  def update
    @project.attributes = params.permit(:name, :description)

    unless @project.valid?
      render_error
      return
    end

    result = if params[:file]
      @project.save_with_file(params[:file])
    else
      @project.save
    end

    if result
      render_success
    else
      render_error
    end
  end

  def destroy
    if @project.destroy_with_file
      render_success
    else
      render_error
    end
  end

  private

  def set_project
    @project = @current_user.projects.find(params[:id])
  end

  def render_success
    view =
      @project.attributes.merge \
        "action" => "#{action_name}_project",
        **@current_user.attributes.slice("username", "display_name", "tagline")

    broadcast "MessagesChannel", view
    render json: view
  end

  def render_error
    error = @project.errors.full_messages.to_sentence
    render json: {error: error}, status: :unprocessable_entity
  end
end
