class Social::ProfilesController < Social::BaseController
  def index
    @users = User.with_klass_name.all.map(&:attributes)
    render json: @users
  end

  def show
    @user = User.with_klass_name.includes(:projects).find(params[:id])
    @projects = @user.projects

    limit = Rails.application.config.project_storage_per_user
    used = @projects.sum(&:size_in_bytes)

    json = @user.attributes.merge(
      projects: @projects,
      storage_limit: limit,
      storage_used: used,
      human_storage_limit: Filesizes.human(limit),
      human_storage_used: Filesizes.human(used),
    )

    render json: json
  end

  def update
    update_params = params.permit(:display_name, :tagline, :password, :password_confirmation)
    if @current_user.update(update_params)
      render json: @current_user.attributes
    else
      render json: {error: @current_user.error_sentences}, status: :unprocessable_entity
    end
  end
end
