class Social::BaseController < ApplicationController
  before_action :authorize_user
end
