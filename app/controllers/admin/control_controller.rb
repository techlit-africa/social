class Admin::ControlController < Admin::BaseController
  def logout
    ad_hoc :sudo, :systemctl, 'restart', 'lightdm'
  end

  def reboot
    ad_hoc :sudo, :systemctl, 'reboot'
  end

  def shutdown
    ad_hoc :sudo, :shutdown, '0'
  end

  private

  def ad_hoc(*cmd)
    on(*client_ips) { execute(*cmd) }

    render json: {}

    if params[:run_on_server]
      on('localhost') { execute(*cmd) }
    end

  rescue SSHKit::Runner::ExecuteError => e
    render json: {error: e.message}, status: 500
  end

  def on(*hosts, &block)
    SSHKit::Coordinator.new(hosts).each(&block)
  end

  def client_ips
    `dhcp-lease-list --parsable`.lines.map do |line|
      line.chomp.split(' ')[3]
    end
  end
end
