class Admin::UsersController < Admin::BaseController
  before_action :set_user, only: %i[update reset_password]

  def index
    render json: User.order(created_at: :desc).all
  end

  def create
    std_action do
      @user = User.create(user_params)
      @user.persisted?
    end
  end

  def update
    std_action do
      @user.update(user_params)
    end
  end

  def reset_password
    std_action do
      @user.update(password: nil)
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.permit(:real_name, :klass_id, :username, :display_name, :tagline)
  end

  def std_action(&block)
    if block[]
      render_success
    else
      render_errors
    end
  end

  def render_success
    render json: @user
  end

  def render_errors
    render json: {error: @user.error_sentences}, status: :unprocessable_entity
  end
end
