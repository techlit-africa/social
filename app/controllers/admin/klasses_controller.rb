class Admin::KlassesController < Admin::BaseController
  before_action :set_klass, only: %i[update destroy]

  def index
    render json: Klass.all
  end

  def create
    std_action do
      @klass = Klass.create(klass_params)
      @klass.persisted?
    end
  end

  def update
    std_action do
      @klass.update(klass_params)
    end
  end

  def destroy
    std_action do
      @klass.destroy
    end

  rescue ActiveRecord::InvalidForeignKey
    error = 'Remove students from this class before deleting it'
    render json: {error: error}, status: :unprocessable_entity
  end

  private

  def set_klass
    @klass = Klass.find(params[:id])
  end

  def klass_params
    params.permit(:name, :slug)
  end

  def std_action(&block)
    if block[]
      render_success
    else
      render_errors
    end
  end

  def render_success
    render json: @klass
  end

  def render_errors
    render json: {error: @klass.error_sentences}, status: :unprocessable_entity
  end
end
