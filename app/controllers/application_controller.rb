class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  def not_found
    render json: {error: 'API route not found.'}, status: :not_found
  end

  def client_html
    render file: 'public/index.html'
  end

  protected

  def authorize_user
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      @current_user = User.with_klass_name.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound
      render json: {error: 'Wrong username. Does it exist?'}, status: :unauthorized
    rescue JWT::DecodeError
      render json: {error: 'Invalid token. Are you logged in?'}, status: :unauthorized
    end
  end

  def authorize_admin
    authorize_user
    return if performed?
    return if @current_user.klass_name == 'teachers'
    render json: {error: 'Not allowed. This area is for teachers only.'}, status: :not_found
  end

  def broadcast(channel, data)
    ActionCable.server.broadcast(channel, data)
  end
end
