class Typing::TestSubmission < ApplicationRecord
  belongs_to :user

  validates_presence_of :content, :wpm, :typos, :accuracy
end
