class Typing::Exercise < ApplicationRecord
  belongs_to :lesson

  has_one :course, through: :lesson

  has_many :submissions
  has_many :users, through: :submissions

  validates_presence_of \
    :title,
    :cuid,
    :position,
    :content,
    :okay_wpm,
    :okay_typos,
    :good_typos,
    :good_wpm

  def self.all_by_id
    @all_by_id ||= all.group_by(&:id).transform_values(&:first)
  end

  def self.all_by_lesson_id
    @by_lesson_id ||= all.order(:position).group_by(&:lesson_id)
  end

  def self.first_id
    Typing::Lesson.for_home_page.dig(0, :exercises, 0, :id)
  end

  def self.next_exercise_id(lesson_id, exercise_id)
    lesson = Typing::Lesson.all_by_id[lesson_id]
    exercise = all_by_id[exercise_id]

    Typing::Exercise.all_by_lesson_id.dig(lesson_id, exercise.position + 1)&.id ||
      Typing::Lesson.for_home_page.dig(lesson.position + 1, :exercises, 0, :id)
  end

  def self.next_positions(lesson_position, exercise_position)
    if Typing::Lesson.for_home_page.dig(lesson_position, :exercises, exercise_position + 1)
      [lesson_position, exercise_position + 1]
    elsif Typing::Lesson.for_home_page.dig(lesson_position + 1, :exercises, 0)
      [lesson_position + 1, 0]
    else
      []
    end
  end
end
