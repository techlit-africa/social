class Typing::Submission < ApplicationRecord
  belongs_to :exercise
  belongs_to :user

  has_one :course, through: :lesson
  has_one :lesson, through: :exercise

  validates_presence_of :wpm, :typos, :accuracy
  validates :complete, :accurate, :fast, inclusion: [true, false]
end
