class Typing::Course < ApplicationRecord
  has_many :lessons
  has_many :exercises,   through: :lessons
  has_many :submissions, through: :exercises
  has_many :users,       through: :submissions

  validates_presence_of :title, :cuid, :version
end
