class Typing::Lesson < ApplicationRecord
  belongs_to :course

  has_many :exercises
  has_many :submissions, through: :exercises
  has_many :users,       through: :submissions

  validates_presence_of :title, :cuid, :position

  def self.all_by_id
    @all_by_id ||= all.group_by(&:id).transform_values(&:first)
  end

  def self.for_home_page
    @for_home_page ||= order(:position).map do |lesson|
      exercises = Typing::Exercise.all_by_lesson_id[lesson.id].map do |e|
        e.attributes.symbolize_keys
      end

      lesson.attributes.symbolize_keys.merge(exercises: exercises)
    end
  end

  def self.first_id
    for_home_page.dig(0, :id)
  end
end
