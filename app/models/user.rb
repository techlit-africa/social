class User < ApplicationRecord
  has_secure_password validations: false

  belongs_to :klass

  has_many :messages, class_name: 'Social::Message'
  has_many :projects, class_name: 'Social::Project'

  has_many :test_submissions, class_name: 'Typing::TestSubmission'
  has_many :submissions,      class_name: 'Typing::Submission'
  has_many :exercises,        class_name: 'Typing::Exercise', through: :submissions
  has_many :lessons,          class_name: 'Typing::Lesson',   through: :exercises
  has_many :courses,          class_name: 'Typing::Course',   through: :courses

  validates :password, length: {minimum: 3}, if: :password

  validates :klass,        presence: true
  validates :username,     presence: true, length: {minimum: 4, maximum: 24}, uniqueness: true, format: {with: /[a-z0-9-]+/}
  validates :display_name, presence: true, length: {maximum: 32}
  validates :tagline,      presence: true, length: {maximum: 48}

  def self.create_from_login(login_params)
    klass = login_params[:klass]

    display_name = login_params[:username]&.titleize
    display_name += " - class #{klass}" if display_name.length < 8

    tagline = "Just a class #{klass} student."

    create(display_name: display_name, tagline: tagline, **login_params)
  end

  def self.with_klass_name
    joins(:klass).select('users.*', 'klasses.name as klass_name')
  end

  def self.matching(term)
    joins(:klass).where(<<~SQL, term, term, term, term, term).order(updated_at: :desc)
      users.real_name like ? or
        users.username like ? or
        users.display_name like ? or
        users.tagline like ? or
        klasses.name like ?
    SQL
  end

  def authenticate(password)
    super(password) && touch(:updated_at)
  end

  def public_attributes
    attributes.except('password_digest')
  end
end
