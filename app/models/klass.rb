class Klass < ApplicationRecord
  has_many :users

  validates :name, presence: true, uniqueness: true
  validates :slug, presence: true, uniqueness: true, format: {with: /[a-z0-9-]+/}
end
