class Social::Message < ApplicationRecord
  belongs_to :user

  validates :content, presence: true, length: { maximum: 120 }
end
