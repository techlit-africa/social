class Social::Project < ApplicationRecord
  belongs_to :user

  validates :name,          presence: true, length: {maximum: 32}, uniqueness: {scope: :user}
  validates :description,   presence: true, length: {maximum: 120}
  validates :storage_path,  presence: true
  validates :content_type,  presence: true
  validates :size_in_bytes, presence: true, numericality: {positive: true}

  def self.storage_path_for(user, name, extension)
    root = Rails.application.config.project_storage_root
    "#{root}/#{user.username}/#{name.parameterize}.#{extension}"
  end

  def download_path
    "/users/#{user.username}/#{File.basename(storage_path)}"
  end

  def attributes
    super.merge(
      "download_path" => download_path,
      "human_size" => human_size,
      "filename" => filename,
    )
  end

  def save_with_file(io)
    FileUtils.mkdir_p(File.dirname(storage_path), mode: 0774)

    limit = Rails.application.config.project_storage_per_user
    user_dir = File.dirname(File.dirname(storage_path))
    used = `du -s #{user_dir}`.split(' ').first.to_i * 1000

    if used + size_in_bytes > (limit * 0.95)
      error = "Not enough storage space! "
      error << "This file is #{human_size} and "
      if limit > used
        error << "you only have #{Filesizes.human(limit - used)} available. "
      else
        error << "your #{Filesizes.human(limit)} is full. "
      end
      error << "Delete files to continue."

      errors.add :base, error
      return false
    end

    File.open(storage_path, "wb") do |f|
      f.write(io.read)
    end

    save && touch(:created_at)
  end

  def destroy_with_file
    FileUtils.rm(storage_path) && destroy
  end

  def human_size
    Filesizes.human size_in_bytes
  end

  def filename
    File.basename storage_path
  end

  def production_ip
    @@production_ip ||= JSON.parse(File.read('/data/techlit/config.json'))['serverIp']
  end
end
