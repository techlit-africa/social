class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  def self.sample(n = nil)
    records = order('random()').limit(n).all
    n ? records : records.first
  end

  def error_sentences
    errors.full_messages.to_sentence
  end
end
