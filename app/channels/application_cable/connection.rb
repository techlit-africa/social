module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = authorize_user
    end

    private

    def authorize_user
      token = request.params[:token]
      reject_unauthorized_connection unless token

      json = JsonWebToken.decode(token)
      reject_unauthorized_connection unless json

      user = User.find_by(id: json[:user_id])
      reject_unauthorized_connection unless user

      user
    end
  end
end
