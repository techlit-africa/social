const {
  override,
  disableEsLint,
  addBabelPlugins,
  addPostcssPlugins,
  addWebpackPlugin,
} = require('customize-cra')

const webpack = require('webpack')

module.exports = override(
  disableEsLint(),

  ...addBabelPlugins(
    'transform-jsx-classname-components',
    'transform-react-pug',
  ),

  addPostcssPlugins([
    require('autoprefixer'),
    require('tailwindcss'),
  ]),

  addWebpackPlugin(new webpack.ProvidePlugin({
    React: 'react',
    Router: 'react-router-dom',
    Switch: ['react-router-dom', 'Switch'],
    Route: ['react-router-dom', 'Route'],
    Redirect: ['react-router-dom', 'Redirect'],
    Link: ['react-router-dom', 'Link'],
    ErrorBoundary: ['react-error-boundary', 'ErrorBoundary'],
    Icon: ['@fortawesome/react-fontawesome', 'FontAwesomeIcon'],
  })),

  addWebpackPlugin(new webpack.DefinePlugin({
    NODE_ENV: `"${process.env.NODE_ENV || 'development'}"`,
  })),
)
