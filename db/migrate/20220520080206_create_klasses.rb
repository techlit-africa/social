class CreateKlasses < ActiveRecord::Migration[7.0]
  def up
    create_table :klasses do |t|
      t.string :name, null: false, unique: true
      t.string :slug, null: false, unique: true
      t.timestamps
    end

    rename_column :users, :klass, :original_klass
    add_reference :users, :klass, foreign_key: true

    if User.any?
      create_klasses_from_users!
      assign_new_klass_ids!
    end

    change_column :users, :klass_id, :integer, null: false
    add_column :users, :real_name, :string
  end

  def down
    remove_column :users, :real_name
    remove_column :users, :klass_id
    rename_column :users, :original_klass, :klass
    drop_table :klasses
  end

  private

  def create_klasses_from_users!
    ts = DateTime.now.strftime('yyyy-mm-dd')

    values =
      execute('select distinct original_klass from users')
        .map { _1['original_klass'] }
        .map { "('#{_1}', '#{_1.parameterize}', '#{ts}', '#{ts}')" }
        .join(', ')

    execute <<~SQL
      insert into klasses (name, slug, created_at, updated_at)
      values #{values}
    SQL
  end

  def assign_new_klass_ids!
    ids =
      execute('select * from klasses').each_with_object({}) do |row, obj|
        obj[row['name']] = row['id']
      end

    execute('select id, original_klass from users').each do |user|
      execute <<~SQL
        update users
        set klass_id = #{ids[user['original_klass']]}
        where id = #{user['id']}
      SQL
    end
  end
end
