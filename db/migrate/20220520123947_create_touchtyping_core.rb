class CreateTouchtypingCore < ActiveRecord::Migration[7.0]
  def up
    path = Rails.root.join('lib', 'typing', 'course_versions', 'touchtyping-core-1.1.json')
    course_attrs = JSON.parse(File.read(path))
    lessons = course_attrs.delete('lessons')
    course = Typing::Course.create!(course_attrs)

    lessons.each do |lesson_attrs|
      exercises = lesson_attrs.delete('exercises')
      lesson = Typing::Lesson.create!(course: course, **lesson_attrs)

      exercises.each do |exercise_attrs|
        Typing::Exercise.create! \
          lesson: lesson,
          cuid: exercise_attrs['cuid'],
          position: exercise_attrs['position'],
          title: exercise_attrs['title'],
          content: exercise_attrs['content'],
          okay_typos: exercise_attrs['okayTypos'],
          okay_wpm: exercise_attrs['okayWpm'],
          good_typos: exercise_attrs['goodTypos'],
          good_wpm: exercise_attrs['goodWpm']
      end
    end
  end
end
