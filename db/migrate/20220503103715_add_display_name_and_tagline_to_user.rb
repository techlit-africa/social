class AddDisplayNameAndTaglineToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :display_name, :string, null: false
    add_column :users, :tagline, :string, null: false, default: "Couldn't think of a tagline"
  end
end
