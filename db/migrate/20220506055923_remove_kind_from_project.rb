class RemoveKindFromProject < ActiveRecord::Migration[7.0]
  def change
    remove_column :projects, :kind
  end
end
