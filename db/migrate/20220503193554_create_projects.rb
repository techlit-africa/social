class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.references :user, null: false, foreign_key: true
      t.string :kind, null: false
      t.string :name, null: false
      t.string :description, null: false
      t.string :content_type, null: false
      t.string :storage_path, null: false
      t.integer :size_in_bytes, null: false

      t.timestamps
    end
  end
end
