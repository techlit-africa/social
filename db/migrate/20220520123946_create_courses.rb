class CreateCourses < ActiveRecord::Migration[7.0]
  def change
    create_table :courses do |t|
      t.string :cuid, null: false
      t.string :title, null: false
      t.string :version, null: false
      t.timestamps
    end

    create_table :lessons do |t|
      t.references :course, null: false, foreign_key: true
      t.string :cuid, null: false
      t.integer :position, null: false
      t.string :title, null: false
      t.timestamps
    end

    create_table :exercises do |t|
      t.references :lesson, foreign_key: true
      t.string :cuid, null: false
      t.integer :position, null: false
      t.string :title, null: false
      t.string :content, null: false
      t.integer :okay_typos, null: false
      t.integer :okay_wpm, null: false
      t.integer :good_typos, null: false
      t.integer :good_wpm, null: false
      t.timestamps
    end

    create_table :submissions do |t|
      t.references :exercise, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :wpm, null: false
      t.integer :typos, null: false
      t.decimal :accuracy, null: false
      t.boolean :complete, default: false, null: false
      t.boolean :accurate, default: false, null: false
      t.boolean :fast, default: false, null: false
      t.timestamps
    end

    create_table :test_submissions do |t|
      t.references :user, foreign_key: true
      t.text :content, null: false
      t.integer :wpm, null: false
      t.integer :typos, null: false
      t.decimal :accuracy, null: false
      t.timestamps
    end
  end
end
