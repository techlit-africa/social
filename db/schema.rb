# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_05_20_123947) do
  create_table "courses", force: :cascade do |t|
    t.string "cuid", null: false
    t.string "title", null: false
    t.string "version", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "exercises", force: :cascade do |t|
    t.integer "lesson_id"
    t.string "cuid", null: false
    t.integer "position", null: false
    t.string "title", null: false
    t.string "content", null: false
    t.integer "okay_typos", null: false
    t.integer "okay_wpm", null: false
    t.integer "good_typos", null: false
    t.integer "good_wpm", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_exercises_on_lesson_id"
  end

  create_table "klasses", force: :cascade do |t|
    t.string "name", null: false
    t.string "slug", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lessons", force: :cascade do |t|
    t.integer "course_id", null: false
    t.string "cuid", null: false
    t.integer "position", null: false
    t.string "title", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_lessons_on_course_id"
  end

  create_table "messages", force: :cascade do |t|
    t.integer "user_id", null: false
    t.text "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "name", null: false
    t.string "description", null: false
    t.string "content_type", null: false
    t.string "storage_path", null: false
    t.integer "size_in_bytes", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "submissions", force: :cascade do |t|
    t.integer "exercise_id"
    t.integer "user_id"
    t.integer "wpm", null: false
    t.integer "typos", null: false
    t.decimal "accuracy", null: false
    t.boolean "complete", default: false, null: false
    t.boolean "accurate", default: false, null: false
    t.boolean "fast", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["exercise_id"], name: "index_submissions_on_exercise_id"
    t.index ["user_id"], name: "index_submissions_on_user_id"
  end

  create_table "test_submissions", force: :cascade do |t|
    t.integer "user_id"
    t.text "content", null: false
    t.integer "wpm", null: false
    t.integer "typos", null: false
    t.decimal "accuracy", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_test_submissions_on_user_id"
  end

# Could not dump table "users" because of following StandardError
#   Unknown type '' for column 'string'

  add_foreign_key "exercises", "lessons"
  add_foreign_key "lessons", "courses"
  add_foreign_key "messages", "users"
  add_foreign_key "projects", "users"
  add_foreign_key "submissions", "exercises"
  add_foreign_key "submissions", "users"
  add_foreign_key "test_submissions", "users"
  add_foreign_key "users", "klasses"
end
