teachers = Klass.where(name: 'teachers', slug: 'teachers').first_or_create!

User.where(username: 'techlit').first_or_create!(
  password: 'empowermogotio',
  display_name: 'TechLit Admin',
  tagline: 'Digital skills unlock global opportunity',
  klass: teachers,
)

#if Rails.env.development?
#  7.times do |klass|
#    [%w[North South Central], %w[Green Blue Yellow], %w[A B C D]].sample.each do |room|
#      (rand(3) + 1).times.map{"G#{_1}"}.each do |group|
#        (rand(15) + 10).times do
#          User.create(
#            klass: [klass, room, group].compact.join(" "),
#            username: FFaker::Internet.user_name.gsub("_","-"),
#            display_name: "#{FFaker::Name.first_name} #{FFaker::Name.last_name}",
#            password: 'asdfasdf',
#          )
#        end
#      end
#    end
#  end
#end
