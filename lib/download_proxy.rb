class DownloadProxy < Rack::Proxy
  def initialize(app)
    @app = app
  end

  def call(env)
    unless env['PATH_INFO'] =~ %r{^/users}
      return @app.call(env)
    end

    unless Rails.env.production?
      env['PATH_INFO'] = '/download' + env['PATH_INFO']
      return @app.call(env)
    end

    env['HTTP_HOST'] = lighttpd_host
    env['HTTP_COOKIE'] = ''

    status, env, body = perform_request(env)

    env['Content-Length'] = (body[0]&.bytesize || 0).to_s

    [status, env, body]
  end

  private

  def lighttpd_host
    @@lighttpd_host ||=
      JSON.parse(File.read('/data/techlit/config.json'))['serverIp']
  end
end
