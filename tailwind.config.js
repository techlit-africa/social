module.exports = {
  separator: '_',
  purge: ['./src/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  variants: {
    extend: {
      textColor: ['disabled', 'active'],
      borderColor: ['disabled', 'active'],
      backgroundColor: ['disabled', 'active'],
    },
  },
  plugins: [],
}
