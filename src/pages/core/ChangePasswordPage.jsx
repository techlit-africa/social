import Button from 'comps/core/Button'
import Input from 'comps/core/Input'
import Layout from 'comps/core/Layout'
import useAuth from 'hooks/core/useAuth'

const PASSWORD_ID = 'password-field'
const PASSWORD_CONFIRMATION_ID = 'password-confirmation-field'

const usePasswordChangeForm = () => {
  //
  // State & other hooks
  //
  const {user, fetch, logout} = useAuth()
  const history = Router.useHistory()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      error: null,
      disabled: false,
      password: '',
      passwordConfirmation: '',
      showPassword: false,
      showPasswordConfirmation: false,
    }
  )

  //
  // Effects
  //
  React.useEffect(() => {
    setTimeout(() => document.getElementById(PASSWORD_ID)?.focus(), 100)
  }, [])

  React.useEffect(() => {
    if (state.password.length < 6)
      return update({error: 'Password is too short!'})

    if (state.password == '')
      return update({error: 'You need a password, it cannot be empty'})

    if (state.password != state.passwordConfirmation)
      return update({error: `Passwords don't match. Are they correct?`})

    update({error: null})
  }, [state.password, state.passwordConfirmation])

  //
  // Callbacks
  //
  const onPasswordChange = (e) => update({password: e.target.value})
  const onPasswordConfirmationChange = (e) => update({passwordConfirmation: e.target.value})

  const submit = () => {
    if (state.password == '') return update({error: 'You need a password, it cannot be empty'})

    update({disabled: true})

    const body = JSON.stringify({
      password: state.password,
      password_confirmation: state.password_confirmation,
    })

    fetch(`/social/profiles/${user.id}`, {method: 'PATCH', body})
      .then(({error, token}) => {
        if (error) throw error
        logout()
      })
      .catch((error) => update({disabled: false, error}))
  }

  const submitIfEnter = (e) => e.key == 'Enter' && submit()

  const togglePassword = () => update({showPassword: !state.showPassword})
  const togglePasswordConfirmation = () => update({showPasswordConfirmation: !state.showPasswordConfirmation})

  //
  // Export
  //
  return {
    ...state, update,
    onPasswordChange, onPasswordConfirmationChange,
    togglePassword, togglePasswordConfirmation,
    submit, submitIfEnter,
  }
}

export default () => pug`
  - const form = usePasswordChangeForm()

  Layout(scratch)
    .w-full.max-w-xs.mx-auto
      .font-medium.text-2xl Make a New Password
      .h-6

      label.text-gray-600 Password
      .h-2
      .relative
        Input.w-full.pr-10(
          id=PASSWORD_ID
          type=(form.showPassword ? 'text' : 'password')
          placeholder=(form.showPassword ? 'p@s$w0rd' : '•••••••')
          value=form.password
          onChange=form.onPasswordChange
          onKeyUp=form.submitIfEnter
          disabled=form.disabled
        )
        button.absolute.top-0.right-0.bottom-0.w-12(
          disabled=form.disabled
          onClick=form.togglePassword
        )
          Icon.text-xl(icon=(form.showPassword ? 'eye' : 'eye-slash'))
      .h-6

      label.text-gray-600 Password Confirmation
      .h-2
      .relative
        Input.w-full.pr-10(
          id=PASSWORD_ID
          type=(form.showPasswordConfirmation ? 'text' : 'password')
          placeholder=(form.showPasswordConfirmation ? 'p@s$w0rd' : '•••••••')
          value=form.password_confirmation
          onChange=form.onPasswordConfirmationChange
          onKeyUp=form.submitIfEnter
          disabled=form.disabled
        )
        button.absolute.top-0.right-0.bottom-0.w-12(
          disabled=form.disabled
          onClick=form.togglePasswordConfirmation
        )
          Icon.text-xl(icon=(form.showPasswordConfirmation ? 'eye' : 'eye-slash'))
      .h-6

      if form.error
        .text-red-500= form.error

      Button.w-full(
        onClick=form.submit
        disabled=form.disabled
      ) Change Password
`
