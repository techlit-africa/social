import Button from 'comps/core/Button'
import Input from 'comps/core/Input'
import Layout from 'comps/core/Layout'
import useAuth from 'hooks/core/useAuth'

const USERNAME_ID = 'username-field'
const PASSWORD_ID = 'password-field'

const useLoginForm = () => {
  //
  // State & other hooks
  //
  const {user, redirect, fetch, login} = useAuth()
  const history = Router.useHistory()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      error: null,
      disabled: false,
      username: '',
      password: '',
      showPassword: false,
    }
  )

  //
  // Effects
  //
  React.useEffect(() => {
    setTimeout(() => document.getElementById(USERNAME_ID)?.focus(), 100)
  }, [])

  React.useEffect(() => {
    if (!user) return
    if (!user.password_digest) return history.push('/change-password')
    history.push(redirect || '/social/feed')
  }, [user])

  //
  // Callbacks
  //
  const onPasswordChange = (e) => update({password: e.target.value})

  const onUsernameChange = (e) => update({
    username: e.target.value.replaceAll(/[^a-zA-Z0-9-]+/g, '-').toLowerCase(),
  })

  const submit = () => {
    update({disabled: true})

    login(state.username, state.password)
      .catch((error) => update({disabled: false, error}))
  }

  const submitIfEnter = (e) => e.key == 'Enter' && submit()

  const togglePassword = () => update({showPassword: !state.showPassword})

  //
  // Export
  //
  return {
    ...state, update,
    onUsernameChange, onPasswordChange,
    submit, submitIfEnter, togglePassword,
  }
}

export default () => pug`
  - const form = useLoginForm()

  Layout(scratch)
    div
      .w-full.max-w-xs.mx-auto
        label.text-gray-600 Username
        .h-2
        Input.font-mono.w-full(
          id=USERNAME_ID
          placeholder='my-username'
          value=form.username
          onChange=form.onUsernameChange
          onKeyUp=form.submitIfEnter
          disabled=form.disabled
        )
        .h-6

        label.text-gray-600 Password
        .h-2
        .relative
          Input.w-full.pr-10(
            id=PASSWORD_ID
            type=(form.showPassword ? 'text' : 'password')
            placeholder=(form.showPassword ? 'p@s$w0rd' : '•••••••')
            value=form.password
            onChange=form.onPasswordChange
            onKeyUp=form.submitIfEnter
            disabled=form.disabled
          )
          button.absolute.top-0.right-0.bottom-0.w-12(
            disabled=form.disabled
            onClick=form.togglePassword
          )
            Icon.text-xl(icon=(form.showPassword ? 'eye' : 'eye-slash'))
        .h-6

        if form.error
          .text-red-500= form.error

        Button.w-full(
          onClick=form.submit
          disabled=form.disabled
        ) Login
`
