import fuzzysearch from 'fuzzysearch'

import Button from 'comps/core/Button'
import useSwr from 'hooks/core/useSwr'
import useModal from 'hooks/core/useModal'
import useGroupedBy from 'hooks/core/useGroupedBy'
import useSortedBy from 'hooks/core/useSortedBy'

import Layout from 'comps/admin/Layout'
import AddKlass from 'modals/admin/AddKlass'
import EditKlass from 'modals/admin/EditKlass'

export default () => pug`
  Layout: Ready
`

const useKlasses = () => {
  const {data: klassesWithoutUserCount} = useSwr('/admin/klasses')
  const {data: users} = useSwr('/admin/users')

  const [filter, setFilter] = React.useState('')

  const usersByKlassId = useGroupedBy(users, ({klass_id}) => klass_id)

  const sortedKlasses = useSortedBy(klassesWithoutUserCount, ({name: a}, {name: b}) => a.localeCompare(b))

  const klasses =
    React.useMemo(() => {
      return sortedKlasses.map((klass) => ({
        ...klass,
        user_count: usersByKlassId[klass?.id]?.length || 0,
      }))
    }, [sortedKlasses, usersByKlassId])

  const filteredKlasses =
    React.useMemo(() => {
      return klasses.filter((klass) => {
        return filter == '' ||
          fuzzysearch(filter, klass.name.toLowerCase()) ||
          fuzzysearch(filter, klass.slug.toLowerCase())
      })
    }, [klasses, filter])

  return {klasses, filteredKlasses, filter, setFilter}
}

const Ready = () => pug`
  - const {klasses, filteredKlasses, filter, setFilter} = useKlasses()

  - const add = useModal()
  - const edit = useModal()

  .flex.justify-between.pb-4
    .flex.flex-col
      .text-2xl.font-semibold.text-gray-700 #{filteredKlasses.length} / #{klasses.length} Classes

      .grid.grid-cols-2.gap-x-2.pr-2
        input.px-1.border.border-gray-500.rounded(
          placeholder='Filter'
          value=filter
          onChange=(e)=>setFilter(e.target.value)
        )

    .flex.flex-col
      Button.flex.items-center(onClick=()=>add.open())
        Icon(icon='plus')
        .pl-2 Add

  .v-scrollbox(style={maxHeight: '60vh'})
    for klass in filteredKlasses
      .h-8.flex.items-center.mt-2.px-2.border.border-gray-500.rounded.shadow.cursor-pointer.hover_bg-white.hover_shadow-lg(
        key=klass.id
        onClick=(klass.name == 'teachers' ? null : ()=>edit.open(klass))
      )
        .flex-1.font-semibold= klass.name
        .font-medium.text-gray-600 #{klass.user_count} user#{klass.user_count == 1 ? '' : 's'}

  AddKlass(modal=add)
  EditKlass(modal=edit)
`
