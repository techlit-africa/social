import useAuth from 'hooks/core/useAuth'

import Layout from 'comps/admin/Layout'

const BUTTON_CLASSES = 'text-4xl h-16 w-16 flex items-center justify-center border-2 rounded-lg shadow transition duration-100 '
const DISABLED_CLASSES = 'border-gray-600 bg-gray-200 text-gray-600'
const LOGOUT_CLASSES = 'border-green-600 text-green-600 hover_bg-green-600 hover_text-white'
const REBOOT_CLASSES = 'border-yellow-600 text-yellow-600 hover_bg-yellow-600 hover_text-white'
const SHUTDOWN_CLASSES = 'border-red-600 text-red-600 hover_bg-red-600 hover_text-white'

const useControl = () => {
  const {fetch} = useAuth()

  const [state, update] = React.useReducer((s1, s2) => ({...s1, ...s2}), {
    runOnServer: false,
    pending: false,
    error: null,
  })

  const send = (command) => {
    update({pending: command})

    const body = JSON.stringify({run_on_server: state.runOnServer})

    fetch(`/admin/control/${command}`, {method: 'POST', body})
      .then(({error}) => {
        if (error) throw error
        update({pending: false, runOnServer: false})
      })
      .catch((error) => update({pending: false, error}))
  }

  return {...state, update, send}
}

const $btn = (control, title, command, icon, classes, addlClasses = '') => pug`
  .flex.flex-col.items-center
    .text-lg.text-gray-600.font-medium= title
    .h-2
    button(
      className=(BUTTON_CLASSES + (control.pending ? DISABLED_CLASSES : classes) + addlClasses)
      disabled=control.pending
      onClick=()=>control.send(command)
    )
      Icon(
        className=(control.pending == command ? 'animate-spin' : '')
        icon=(control.pending == command ? 'circle-notch' : icon)
      )
`

export default () => pug`
  - const control = useControl()

  Layout
    .flex.justify-around.py-6.border-b.border-gray-500
      = $btn(control, 'Logout', 'logout', 'power-off', LOGOUT_CLASSES, ' transform -rotate-90')
      = $btn(control, 'Reboot', 'reboot', 'undo', REBOOT_CLASSES)
      = $btn(control, 'Shutdown', 'shutdown', 'power-off', SHUTDOWN_CLASSES)

    .flex.items-center.justify-between.pt-2
      .text-lg.text-red-500.font-medium= control.error

      button.flex.items-center.pl-2(
        disabled=control.pending
        onClick=()=>control.update({runOnServer: !control.runOnServer})
      )
        .font-medium(
          className=(control.runOnServer ? 'text-blue-600' : 'text-gray-600')
        ) Include Server
        .w-2
        .text-2xl.h-10.w-10.items-center.justify-center.border-2.rounded-lg.shadow-sm(
          className=(control.runOnServer ? 'bg-blue-600 text-white' : 'border-gray-400 text-gray-400')
        )
          Icon(icon='check')
`
