import {mutate} from 'swr'

import Button from 'comps/core/Button'
import InputField from 'comps/core/InputField'
import SelectField from 'comps/core/SelectField'
import useAuth from 'hooks/core/useAuth'
import useSwr from 'hooks/core/useSwr'
import useFocusOnMount from 'hooks/core/useFocusOnMount'
import useGroupedBy from 'hooks/core/useGroupedBy'
import useSortedBy from 'hooks/core/useSortedBy'

const REAL_NAME_INPUT_ID    = 'real-name-input'
const KLASS_INPUT_ID        = 'klass-input'
const USERNAME_INPUT_ID     = 'username-input'
const DISPLAY_NAME_INPUT_ID = 'display-name-input'
const TAGLINE_INPUT_ID      = 'tagline-input'

const formInit = (user = {}) => ({
  real_name: user.real_name || '',
  klass_id: user.klass_id || '',

  username: user.username || '',
  username_touched: !!user.username,

  display_name: user.display_name || '',
  display_name_touched: !!user.display_name,

  tagline: user.tagline || '',
  tagline_touched: !!user.tagline,
})

const useForm = ({user, onClose}) => {
  const isNew = !user

  const {fetch} = useAuth()
  const {data: unsortedKlasses} = useSwr('/admin/klasses')

  const klasses = useSortedBy(unsortedKlasses, ({name: a}, {name: b}) => a.localeCompare(b))
  const klassesById = useGroupedBy(unsortedKlasses, ({id}) => id)

  const focus = useFocusOnMount(REAL_NAME_INPUT_ID)

  const [state, update] = React.useReducer((s1, s2) => ({...s1, ...s2}), formInit(user))

  React.useEffect(() => {
    const changes = {}

    const klass_name = klassesById[state.klass_id]?.[0]?.name

    if (!state.username_touched) {
      changes.username = state.real_name.replaceAll(' ', '-').toLowerCase()
    }

    if (!state.display_name_touched) {
      changes.display_name = klass_name ? `${state.real_name} | ${klass_name}` : state.real_name
      if (changes.display_name.length > 32) changes.display_name = state.real_name
    }

    if (!state.tagline_touched) {
      changes.tagline = ['Just a', klass_name, 'student'].filter(x => x).join(' ')
    }

    update(changes)
  }, [state.real_name, state.klass_id])

  const onChange = (field) => (e) => update({
    [field]: e.target.value,
    [`${field}_touched`]: e.target.value != '',
  })

  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    update({disabled: true})


    const body = {
      real_name: state.real_name,
      klass_id: state.klass_id,
      username: state.username,
      display_name: state.display_name,
      tagline: state.tagline,
    }

    if (isNew) body.password = body.username

    const [method, path] = isNew
      ? ['POST', `/admin/users`]
      : ['PATCH', `/admin/users/${user.id}`]

    fetch(path, {method, body: JSON.stringify(body)})
      .then(({error, ...user}) => {
        setTimeout(() => {
          if (error) return setError(error)

          mutate('/admin/users', (users = []) => isNew
            ? [user, ...users]
            : users.map((u) => u.id == user.id ? user : u)
          )

          setTimeout(() => {
            if (onClose) return onClose()

            update(formInit(user))
            focus()
          })
        })
      })
      .catch(setError)
  }

  const submitIfEnter = (e) => e.keyCode == 13 && submit()

  return {isNew, ...state, update, onChange, submit, submitIfEnter, klasses}
}

export default ({user, onClose}) => pug`
  - const form = useForm({user, onClose})

  SelectField(
    id=KLASS_INPUT_ID
    label='Class'
    placeholder='Class'
    value=form.klass_id
    onChange=form.onChange('klass_id')
    onKeyUp=form.submitIfEnter
    disabled=form.disabled
  )
    option(value='') -- Select a Class --
    for klass in form.klasses
      option(key=klass.id value=klass.id)= klass.name
  .h-4

  if form.klass_id != ''
    InputField(
      id=REAL_NAME_INPUT_ID
      label='Real Name'
      placeholder='Real Name'
      value=form.real_name
      onChange=form.onChange('real_name')
      onKeyUp=form.submitIfEnter
      disabled=form.disabled
    )
    .h-4

    InputField(
      id=USERNAME_INPUT_ID
      label='Username'
      placeholder='username'
      value=form.username
      onChange=form.onChange('username')
      onKeyUp=form.submitIfEnter
      disabled=(form.disabled || !form.isNew)
    )
    .h-4

    InputField(
      id=DISPLAY_NAME_INPUT_ID
      label='Display Name'
      placeholder='Display Name'
      value=form.display_name
      onChange=form.onChange('display_name')
      onKeyUp=form.submitIfEnter
      disabled=form.disabled
    )
    .h-4

    InputField(
      id=TAGLINE_INPUT_ID
      label='Tagline'
      placeholder='Tagline'
      value=form.tagline
      onChange=form.onChange('tagline')
      onKeyUp=form.submitIfEnter
      disabled=form.disabled
    )
    .h-4

  if form.error
    .text-red-500= form.error

  Button.w-full(
    onClick=form.submit
    disabled=form.disabled
  ) Save
`
