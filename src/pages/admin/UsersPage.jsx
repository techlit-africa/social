import dayjs from 'dayjs'
import fuzzysearch from 'fuzzysearch'

import Avatar from 'comps/core/Avatar'
import Button from 'comps/core/Button'
import useSwr from 'hooks/core/useSwr'
import useModal from 'hooks/core/useModal'
import useGroupedBy from 'hooks/core/useGroupedBy'
import useSortedBy from 'hooks/core/useSortedBy'

import Layout from 'comps/admin/Layout'
import AddUser from 'modals/admin/AddUser'
import EditUser from 'modals/admin/EditUser'

export default () => pug`
  Layout: Ready
`

const useUsers = () => {
  const {data: klasses} = useSwr('/admin/klasses')
  const {data: usersWithoutKlass} = useSwr('/admin/users')

  const [klassId, setKlassId] = React.useState('')
  const [filter, setFilter] = React.useState('')

  const klassesById = useGroupedBy(klasses, ({id}) => id)

  const users = React.useMemo(() => {
    return usersWithoutKlass.map((user) => ({
      ...user,
      klass_slug: klassesById[user.klass_id][0].slug,
    }))
  }, [usersWithoutKlass, klassesById])

  const filteredUsers = React.useMemo(() => {
    return users.filter((user) => {
      if (klassId != '' && user.klass_id != klassId) return false

      return filter == '' ||
        fuzzysearch(filter, klassesById[user.klass_id][0].name.toLowerCase()) ||
        fuzzysearch(filter, user.real_name?.toLowerCase() || '') ||
        fuzzysearch(filter, user.username) ||
        fuzzysearch(filter, user.display_name.toLowerCase()) ||
        fuzzysearch(filter, user.tagline.toLowerCase())
    })
  }, [users, klassId, filter])

  return {users, filteredUsers, klassId, setKlassId, filter, setFilter, klasses}
}

const Ready = () => pug`
  - const {users, filteredUsers, klassId, setKlassId, filter, setFilter, klasses} = useUsers()

  - const add = useModal()
  - const edit = useModal()

  .flex.justify-between.pb-4
    .flex.flex-col
      .text-2xl.font-semibold.text-gray-700 #{filteredUsers.length} / #{users.length} Users

      .grid.grid-cols-2.gap-x-2.pr-2
        select.px-1.border.border-gray-500.rounded(
          value=klassId
          onChange=(e)=>setKlassId(e.target.value)
        )
          option(value='') All Classes
          for klass in klasses
            option(key=klass.id value=klass.id)= klass.name

        input.px-1.border.border-gray-500.rounded(
          placeholder='Filter'
          value=filter
          onChange=(e)=>setFilter(e.target.value)
        )

    .flex.flex-col
      Button.flex.items-center(onClick=()=>add.open())
        Icon(icon='plus')
        .pl-2 Add

  .v-scrollbox(style={maxHeight: '60vh'})
    for user in filteredUsers
      // User container
      .flex.mt-2.p-1.border.border-gray-500.rounded.shadow.cursor-pointer.hover_bg-white.hover_shadow-lg(
        key=user.id
        onClick=(user.username == 'techlit' ? null : ()=>edit.open(user))
      )
        // User avatar
        Avatar(sm profile=user)

        // A column of text
        .flex.flex-col.pl-2.leading-tight
          // Display name
          .text-black(style={fontSize: '0.85em'})= user.display_name
          // Tagline
          .text-gray-800(style={fontSize: '0.75em'})= user.tagline
          // Context
          .flex(style={fontSize: '0.5em'})
            .text-green-600.font-mono #{user.klass_slug}/#{user.username}
            .pl-2.text-gray-600 Logged in #{dayjs(user.updated_at).fromNow()}.

  AddUser(modal=add)
  EditUser(modal=edit)
`
