import {mutate} from 'swr'

import Button from 'comps/core/Button'
import InputField from 'comps/core/InputField'
import useAuth from 'hooks/core/useAuth'
import useFocusOnMount from 'hooks/core/useFocusOnMount'

const NAME_INPUT_ID = 'name-input'
const SLUG_INPUT_ID = 'slug-input'

const formInit = (klass = {}) => ({
  name: klass.name || '',
  slug: klass.slug || '',
})

const useForm = ({klass, onClose}) => {
  const {fetch} = useAuth()

  const focus = useFocusOnMount(NAME_INPUT_ID)

  const [state, update] = React.useReducer((s1, s2) => ({...s1, ...s2}), formInit(klass))

  React.useEffect(() => {
    update({slug: state.name.replaceAll(' ', '-').toLowerCase()})
  }, [state.name])

  const onChange = (field) => (e) => update({[field]: e.target.value})
  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    update({disabled: true})

    const body = JSON.stringify({
      name: state.name,
      slug: state.slug,
    })

    const [method, path] = klass
      ? ['PATCH', `/admin/klasses/${klass.id}`]
      : ['POST', `/admin/klasses`]

    fetch(path, {method, body})
      .then(({error}) => {
        setTimeout(() => {
          if (error) return setError(error)

          mutate('/admin/klasses', (klasses = []) => klass
            ? klasses.map((k) => k.id == klass.id ? klass : k)
            : [klass, ...klasses]
          )

          setTimeout(() => {
            if (onClose) return onClose()

            update(formInit(klass))
            focus()
          })
        })
      })
      .catch(setError)
  }

  const submitIfEnter = (e) => e.keyCode == 13 && submit()

  return {...state, update, onChange, submit, submitIfEnter}
}

export default ({klass, onClose}) => pug`
  - const form = useForm({klass, onClose})

  InputField(
    id=NAME_INPUT_ID
    label='Name'
    placeholder='Name & Group'
    value=form.name
    onChange=form.onChange('name')
    onKeyUp=form.submitIfEnter
    disabled=form.disabled
  )
  .h-4

  InputField(
    id=SLUG_INPUT_ID
    label='Slug'
    placeholder='name-group'
    value=form.slug
    onChange=form.onChange('slug')
    onKeyUp=form.submitIfEnter
    disabled=form.disabled
  )
  .h-4

  if form.error
    .text-red-500= form.error

  Button.w-full(
    onClick=form.submit
    disabled=form.disabled
  ) Save
`
