import useSwr from 'hooks/core/useSwr'
import useKeysDown from 'hooks/core/useKeysDown'

import Layout from 'comps/typing/Layout'

export default () => pug`
  Layout(left='back' Title=ReadyTitle): Ready
`

const useSubmission = () => {
  const {id} = Router.useParams()
  const {data: submission} = useSwr(id ? `/typing/submissions/${id}` : null)
  return submission
}

const ReadyTitle = () => pug`
  - const submission = useSubmission()

  span.flex.flex-row.items-center.justify-center
    span.pr-3.text-base.text-gray-600.dark_text-gray-400 #{submission.lesson.title}:#{' '}
    span.text-gray-900.dark_text-gray-100= submission.exercise.title
`

const Ready = () => {
  const {id} = Router.useParams()
  const history = Router.useHistory()
  const submission = useSubmission()

  const nextPath =
    submission.next_exercise_id
      ? `/typing/exercises/${submission.next_exercise_id}`
      : '/typing/lessons'
  const prevPath = `/typing/exercises/${submission.exercise_id}`

  const keyDownRef = React.useRef()
  keyDownRef.current = (event) => {
    if (event.code === 'Escape') {
      history.push(prevPath)
    }
    if (event.code === 'Enter') {
      history.push(submission.complete ? nextPath : prevPath)
    }
  }

  useKeysDown({
    onKeyDownRef: keyDownRef,
  })

  const [title, subtitle] = messages(submission)

  return pug`
      .w-full.flex-1.flex.flex-col.overflow-y-scroll.items-center.p-8
        .w-full.max-w-screen-md.flex.flex-col.items-center.py-4.border.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow.rounded

          .w-full.flex.justify-between
            Link(to=prevPath)
              button.w-48.flex.flex-col.items-center.text-blue-800.dark_text-blue-200.hover_text-blue-700.dark_hover_text-blue-300.active_text-blue-800.dark_active_text-blue-200.font-medium
                Icon.text-4xl(icon='arrow-alt-circle-left')
                span.text-2xl Try again
                span.text-gray-500 (Escape)

            .text-center.pt-8
              .text-4xl.pb-2.text-gray-900.dark_text-gray-100= title
              .text-xl.pb-12.text-gray-700.dark_text-gray-300= subtitle

            if submission.complete
              Link(to=nextPath)
                button.w-48.flex.flex-col.items-center.text-blue-800.dark_text-blue-200.hover_text-blue-700.dark_hover_text-blue-300.active_text-blue-800.dark_active_text-blue-200.font-medium
                  Icon.text-4xl(icon='arrow-alt-circle-right')
                  span.text-2xl Continue
                  span.text-gray-500 (Enter)
            else
              .w-48

          .flex.items-center.justify-center
            if submission.accurate && submission.fast
              Icon.text-yellow-500(style={fontSize: '8em'} icon='star')

            else
              .px-8
                Icon(
                  style={fontSize: '8em'}
                  icon='check'
                  className=(submission.complete ? 'text-yellow-500' : 'text-gray-500')
                )
              .px-8
                Icon(
                  style={fontSize: '8em'}
                  icon='bullseye'
                  className=(submission.accurate ? 'text-yellow-500' : 'text-gray-500')
                )
              .px-8
                Icon(
                  style={fontSize: '8em'}
                  icon='bolt'
                  className=(submission.fast ? 'text-yellow-500' : 'text-gray-500')
                )

          .py-6.flex.items-end.justify-between(style={width: '18em'})
            .flex.flex-col.items-center
              .text-6xl.text-gray-900.dark_text-gray-100= submission.wpm
              .text-2xl.text-gray-500.font-medium wpm
            .flex.flex-col.items-center
              .text-6xl.text-gray-900.dark_text-gray-100= submission.typos
              .text-2xl.text-gray-500.font-medium typos
  `
}

const messages = ({
  wpm, typos, complete, accurate, fast,
  exercise: {okay_wpm, okay_typos, good_wpm, good_typos},
}) => {
  let title = ''
  let subtitle = ''

  if (fast) {
    title = 'That was fast!'
  } else if (accurate) {
    title = 'That was accurate!'
  } else if (complete) {
    title = 'You passed!'
  } else {
    title = 'Almost there!'
  }

  if (typos > okay_typos) {
    if (okay_typos > 0) {
      subtitle = `You need fewer than ${okay_typos + 1} typos to pass.`
    } else {
      subtitle = "You can't make any mistakes on this exercise."
    }
  } else if (wpm < okay_wpm) {
    subtitle = `You need to type at least ${okay_wpm} words per minute.`
  } else if (typos > good_typos) {
    if (good_typos > 0) {
      subtitle = `Can you do it with fewer than ${good_typos + 1} typos?`
    } else {
      subtitle = 'Can you do it without any typos?'
    }
  } else if (wpm < good_wpm) {
    subtitle = `Can you do it faster than ${good_wpm} words per minute?`
  } else if (wpm >= good_wpm && typos <= good_typos) {
    title = 'You did it!'
    subtitle = '👏🎉👏🎉👏🎉'
  }

  return [title, subtitle]
}
