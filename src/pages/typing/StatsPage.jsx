import HomeLayout from 'comps/typing/HomeLayout'
import useAuth from 'hooks/core/useAuth'
import useSwr from 'hooks/core/useSwr'

export default () => pug`
  HomeLayout: Ready
`

const useStats = () => {
  const {user} = useAuth()
  const {data: stats} = useSwr('/typing/stats')
  const history = Router.useHistory()

  const userStats = stats.users[user.id]

  const goToClass = () => history.push(`/typing/exercises/${userStats ? userStats.next_exercise_id : stats.first_exercise_id}`)
  const goToTest = () => history.push('/typing/test')

  return {stats, userStats, goToClass, goToTest}
}

const Ready = () => pug`
  - const {stats, userStats, goToClass, goToTest} = useStats()

  .grid.grid-cols-2
    .grid.grid-cols-3.gap-2.text-gray-800.dark_text-gray-200
      .col-span-2.text-center.pb-2.text-2xl.text-gray-600.dark_text-gray-400 Progress
      .h-0

      .flex.flex-col.items-center.rounded-xl.border-2.border-gray-300.dark_border-gray-700
        .text-6xl= userStats ? userStats.lesson + 1 : 1
        .text-xl Lesson
      .flex.flex-col.items-center.rounded-xl.border-2.border-gray-300.dark_border-gray-700
        .text-6xl= userStats ? userStats.exercise + 1 : 1
        .text-xl Exercise
      .h-0

      .col-span-3.pt-4
        button.text-2xl.text-blue-500.dark_text-blue-400.hover_text-blue-600.dark_hover_text-blue-500(onClick=goToClass)
          Icon(icon='keyboard')
          span.pl-2 Start next exercise

      .col-span-3.pt-4
        button.text-2xl.text-blue-500.dark_text-blue-400.hover_text-blue-600.dark_hover_text-blue-500(onClick=goToTest)
          Icon(icon='award')
          span.pl-2 Test your WPM


    .grid.grid-cols-3.gap-2.text-gray-800.dark_text-gray-200
      .col-span-3.text-center.text-2xl.text-gray-600.dark_text-gray-400 Achievements

      .h-0
      = badgesEarned('text-6xl', 'star', userStats ? userStats.star : 0, stats.total_badges)
      .h-0

      = badgesEarned('text-6xl', 'check', userStats ? userStats.complete : 0, stats.total_badges)
      = badgesEarned('text-5xl', 'bullseye', userStats ? userStats.accurate : 0, stats.total_badges)
      = badgesEarned('text-5xl', 'bolt', userStats ? userStats.fast : 0, stats.total_badges)

  .my-12.w-full.border-b.border-gray-300.dark_border-gray-700
  .my-8.text-center.text-2xl.text-gray-600.dark_text-gray-400 Leaderboard
  .grid.grid-cols-3.text-xl
    div
      .text-center
        Icon.text-5xl.text-yellow-500(icon='keyboard')
        .text-xl.text-gray-600.dark_text-gray-400.pb-3 Progress
      .pr-1.border-r.border-gray-300.dark_border-gray-700
        for leader, i in stats.progress
          .flex.justify-between.dark_border-gray-700.text-gray-800.dark_text-gray-200(key=leader.username)
            .w-10 #{i + 1}.
            .flex-1.font-mono.truncate= leader.username
            .w-10 #{leader.lesson + 1}.#{leader.exercise + 1}

    div
      .text-center
        Icon.text-5xl.text-yellow-500(icon='award')
        .text-xl.text-gray-600.dark_text-gray-400.pb-3 WPM
      .pl-2.border-r.border-gray-300.dark_border-gray-700
        for leader, i in stats.wpm
          .flex.justify-between.dark_border-gray-700.text-gray-800.dark_text-gray-200(key=leader.username)
            .w-10 #{i + 1}.
            .flex-1.font-mono.truncate= leader.username
            .w-10= leader.max_wpm

    div
      .text-center
        Icon.text-5xl.text-yellow-500(icon='star')
        .text-xl.text-gray-600.dark_text-gray-400.pb-3 Star Student
      .pl-2
        for leader, i in stats.star
          .flex.justify-between.dark_border-gray-700.text-gray-800.dark_text-gray-200(key=leader.username)
            .w-10 #{i + 1}.
            .flex-1.font-mono.truncate= leader.username
            .w-10= leader.star
`

const badgesEarned = (className, icon, earned, total) => pug`
  .relative.flex.items-center.justify-center
    .absolute.inset-0.flex.items-end.justify-end
      .px-1.bg-gray-100.dark_bg-gray-900.rounded-xl.border-2.border-gray-300.dark_border-gray-700
        span.text-xl= earned
        span.text-xs.px-1 /
        span.text-xs= total

    Icon.text-yellow-500(className=className icon=icon)
`
