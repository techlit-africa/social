import dayjs from 'dayjs'

import Avatar from 'comps/core/Avatar'
import useAuth from 'hooks/core/useAuth'
import useModal from 'hooks/core/useModal'

import EditMyProfile from 'modals/social/EditMyProfile'

export default ({profile}) => pug`
  - const {logout} = useAuth()
  - const editMyProfile = useModal()
  - const changePassword = useModal()

  // Header container
  .flex.items-start.justify-between
    // Profile content on the left
    .flex.p-2
      // User avatar
      Avatar(lg profile=profile)

      // A column of text
      .flex.flex-col.pl-4.font-medium.text-gray-600.leading-tight
        // Display name
        .text-3xl.text-black= profile.display_name
        // Tagline
        .text-2xl.text-gray-800= profile.tagline
        // Context
        .flex.text-sm.pt-2
          .text-green-600.font-mono #{profile.klass_name == 'teachers' ? '' : 'class-'}#{profile.klass_name}/#{profile.username}
          .pl-3 Logged in #{dayjs(profile.updated_at).fromNow()}.

    // Profile controls on the right (if my profile)
    .flex.flex-col.text-xl.text-blue-600
      if profile.mine
        // Logout button
        button.px-2(onClick=()=>logout())
          Icon(icon='power-off')

        .h-3
        // Edit profile button
        button.px-2(onClick=editMyProfile.open)
          Icon(icon='pen')

        .h-3
        // Change password button
        Link(to='/change-password')
          button.px-2: Icon(icon='lock')

  // Edit profile modal (if my profile)
  if profile.mine
    EditMyProfile(modal=editMyProfile)
`
