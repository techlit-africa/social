import Button from 'comps/core/Button'
import useModal from 'hooks/core/useModal'

import AddProject from 'modals/social/AddProject'

export default ({profile}) => pug`
  - const addProject = useModal()

  if profile.mine
    .h-8
    .flex.items-center.justify-between.py-2
      .text-gray-600 #{profile.human_storage_used} / #{profile.human_storage_limit}

      .h-4.flex-1.mx-3.rounded-full.bg-gray-300
        if profile.storage_used > profile.storage_limit * 0.95
          .h-full.w-full.bg-red-300.rounded-full.border.border-red-400
        else if profile.storage_used > profile.storage_limit / 100
          .h-full.bg-blue-300.rounded-l-full.border.border-blue-400(
            style={width: ((profile.storage_used / profile.storage_limit) * 100) + '%'}
          )

      Button.flex.items-center(onClick=addProject.open)
        Icon(icon='plus')
        .pl-2 Add

  if profile.mine
    AddProject(modal=addProject)
`
