import dayjs from 'dayjs'

import useModal from 'hooks/core/useModal'

import EditProject from 'modals/social/EditProject'
import ShowProject from 'modals/social/ShowProject'

const projectKind = ({content_type}) =>
  content_type.split('/')[0]

const TABS = {
  all: 'All',
  image: 'Images',
  audio: 'Audio',
  video: 'Video',
  other: 'Other',
}

const useProjects = (profile) => {
  const [activeTab, setActiveTab] = React.useState('all') // all video audio image

  const projectsByTab = React.useMemo(() => {
    const byTab = {all: profile.projects, image: [], audio: [], video: [], other: []}

    profile.projects.forEach((p) => {
      const kind = projectKind(p)
      if (kind == 'image') return byTab.image.push(p)
      if (kind == 'audio') return byTab.audio.push(p)
      if (kind == 'video') return byTab.video.push(p)
      byTab.other.push(p)
    })

    return byTab
  }, [profile, activeTab])

  return {projectsByTab, activeTab, setActiveTab}
}

export default ({profile}) => pug`
  - const {projectsByTab, activeTab, setActiveTab} = useProjects(profile)

  - const edit = useModal()
  - const show = useModal()

  .flex.items-end.border-b.border-gray-400
    for tab in Object.keys(TABS)
      if tab == 'all' || projectsByTab[tab].length
        .pb-1.mr-1(
          key=tab
          className=(tab == activeTab ? 'border-b-2 border-blue-600' : '')
        )
          button.px-3.pb-1.font-bold.text-xl.hover_bg-gray-300.rounded(
            className=(tab == activeTab ? 'text-blue-600' : 'text-gray-700')
            onClick=()=>setActiveTab(tab)
          )
            | #{TABS[tab]} (#{projectsByTab[tab].length})

  .h-4
  unless projectsByTab[activeTab].length > 0
    .h-12.pl-6.flex.items-center.text-gray-600 Nothing here

  else
    .grid.grid-cols-3.gap-3
      for project in projectsByTab[activeTab]
        .border.border-gray-400.rounded.shadow(key=project.id)
          .h-24
            case project.content_type.split('/')[0]
              when 'image'
                button.h-full.w-full.flex.flex-col.items-center.justify-center.bg-checkerboard-1.border-b.border-gray-400.rounded-t.transition.duration-200(
                  onClick=()=>show.open(project)
                )
                  img.h-full.mx-auto(src=project.download_path)

              when 'audio'
                button.h-full.w-full.flex.flex-col.items-center.justify-center.border-b.border-gray-400.rounded-t.transition.duration-200
                  audio.w-full(src=project.download_path controls)

              when 'video'
                button.h-full.w-full.flex.flex-col.items-center.justify-center.border-b.border-gray-400.rounded-t.transition.duration-200
                  video.h-full.mx-auto(src=project.download_path controls)

              default
                button.h-full.w-full.flex.items-center.justify-center.bg-gray-300.border-b.border-gray-400
                  Icon.text-3xl.text-gray-600(icon='file-upload')

          .grid.grid-cols-2.border-b.border-gray-300.text-blue-600
            a.py-1.h-full.text-center.border-r.hover_bg-blue-200.transition(
              href=project.download_path
              download
            )
              Icon(icon='file-download')

            if profile.mine
              button.py-1.h-full.hover_bg-blue-200.transition(
                onClick=()=>edit.open(project)
              )
                Icon(icon='pen')

          .p-2
            .font-bold.text-gray-600.truncate= project.name
            .text-xs.text-gray-600 #{project.human_size} | #{dayjs(project.updated_at).fromNow()}
            .text-gray-800.leading-tight= project.description

  if profile.mine
    EditProject(modal=edit)

  ShowProject(modal=show)
`
