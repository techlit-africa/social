import {Popover} from 'react-tiny-popover'

import Button from 'comps/core/Button'
import Input from 'comps/core/Input'
import useAuth from 'hooks/core/useAuth'
import useFocusOnMount from 'hooks/core/useFocusOnMount'

const MESSAGE_INPUT_ID = 'message-input'

const EMOJI = (
  '😀 😁 😂 🤣 😃 😄 😅 😆 😉 😊 😋 😎 😍 😘 🥰 😗 😙 😚 🙂 🤗 🤩 🤔 🤨 😐 😑 😶 🙄 ' +
  '😏 😣 😥 😮 🤐 😯 😪 😫 😴 😌 😛 😜 😝 🤤 😒 😓 😔 😕 🙃 🤑 😲 🙁 😖 😞 😟 😤 😢 ' +
  '😭 😦 😧 😨 😩 🤯 😬 😰 😱 🥵 🥶 😳 🤪 😵 😡 😠 🤬 😷 🤒 🤕 🤢 🤮 🤧 😇 🤠 🤡 🥳 ' +
  '🥴 🥺 🤥 🤫 🤭 🧐 🤓 😈 👿 👹 👺 💀 👻 👽 🤖 💩 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👋 🤚 ' +
  '🖐 ✋ 🖖 👌 🤏 🤞 🤟 🤘 🤙 👈 👉 👆 🖕 👇 👍 👎 ✊ 👊 🤛 🤜 👏 🙌 👐 🤲 🤝 🙏 ' +
  '💪 🧠 👀 👁 👅 👄 🙇‍♀️ 🙇 🙇‍♂️ 💁‍♀️ 💁 💁‍♂️ 🙅‍♀️ 🙅 🙅‍♂️ 🙆‍♀️ 🙆 🙆‍♂️ 🙋‍♀️ 🙋 🙋‍♂️ 🤦‍♀️ 🤦 🤦‍♂️ ' +
  '🤷‍♀️ 🤷 🤷‍♂️ 🙎‍♀️ 🙎 🙎‍♂️ 🙍‍♀️ 🙍 🙍‍♂️ 💇‍♀️ 💇 💇‍♂️ 💆‍♀️ 💆 💆‍♂️ 🧖‍♀️ 🧖 🧖‍♂️ 💅 🤳 💃 🕺 👯‍♀️ 👯 👯‍♂️ 🕴 ' +
  '🚶‍♀️ 🚶 🚶‍♂️ 🏃‍♀️ 🏃 🏃‍♂️ 👭 🧑‍🤝‍🧑 👬 👫 💫 ⭐️ 🌟 ' +
  '✨ ⚡️ ☄️ 💥 🔥 🌪 🌈 ☀️ 🌤 ⛅️ 🌥 ☁️ 🌦 🌧 ⛈ 🌩 🌨 ❄️ ☃️ ⛄️ 🌬 💨 💧 💦 ☔️ 🚀 🏆 💵 ' +
  '🎉 🙈 🙉 🙊 💌 💘 💝 💖 💗 💓 💞 💕 💔 ❤️ 🧡 💛 💚 💙 💜 🖤 🤍 💯 '
).split(' ')

const formInit = (message = {}) => ({
  content: message.content || '',
  disabled: false,
  error: false,
  showEmoji: false,
})

const useForm = ({message, onClose}) => {
  const {fetch} = useAuth()

  const focus = useFocusOnMount(MESSAGE_INPUT_ID)

  const [state, update] = React.useReducer((s1, s2) => ({...s1, ...s2}), formInit(message))

  const onChangeContent = (e) => update({content: e.target.value})
  const addEmoji = (emoji) => update({content: state.content + emoji})
  const toggleEmoji = () => update({showEmoji: !state.showEmoji})
  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    if (state.content.trim() === '') return

    update({disabled: true})

    const body = JSON.stringify({
      content: state.content,
    })

    const [method, path] = message
      ? ['PATCH', `/social/messages/${message.id}`]
      : ['POST', `/social/messages`]

    fetch(path, {method, body})
      .then(({error}) => {
        setTimeout(() => {
          if (error) return setError(error)

          setTimeout(() => {
            if (onClose) return onClose()

            update(formInit(message))
            focus()
          })
        })
      })
      .catch(setError)
  }

  const submitIfEnter = (e) => e.keyCode == 13 && submit()

  return {
    ...state,
    update,
    onChangeContent,
    toggleEmoji,
    addEmoji,
    submit,
    submitIfEnter,
  }
}

const $emoji = (addEmoji) => pug`
  .w-40.h-40.p-1.bg-white.border.border-gray-300.overflow-y-scroll.rounded-lg.shadow-lg
    each emoji in EMOJI
      button.text-2xl(key=emoji onClick=()=>addEmoji(emoji))= emoji
`

export default ({message, onClose}) => pug`
  - const form = useForm({message, onClose})

  if form.error
    .text-red-500= form.error

  .h-16.flex.items-center
    Popover(
      isOpen=form.showEmoji
      positions=['top']
      align='center'
      padding=9
      content=$emoji(form.addEmoji)
    )
      button.px-2.py-1.border.border-blue-500.rounded.shadow-lg(onClick=form.toggleEmoji)
        .text-2xl 😊

    .w-3
    Input.flex-1(
      id=MESSAGE_INPUT_ID
      disabled=form.disabled
      value=form.content
      onChange=form.onChangeContent
      onKeyUp=form.submitIfEnter
    )

    .w-3
    Button.flex.items-center.px-3(
      disabled=form.disabled
      onClick=form.submit
    )
      if form.disabled
        .text-lg.pr-3 Send
        Icon.animate-spin(icon='circle-notch')
      else
        .text-lg.pr-3 Send
        Icon(icon='paper-plane')
`
