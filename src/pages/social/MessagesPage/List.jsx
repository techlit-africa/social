import {useActionCable} from 'use-action-cable'
import {mutate} from 'swr'
import dayjs from 'dayjs'

import useAuth from 'hooks/core/useAuth'
import useModal from 'hooks/core/useModal'
import useSwr from 'hooks/core/useSwr'

import EditMessage from 'modals/social/EditMessage'

const useMessages = () => {
  const {data: messages} = useSwr('/social/messages')

  React.useEffect(() => {
    const container = document.getElementById('messageList')
    container.scrollTo(0, container.scrollHeight)
  }, [messages])

  const shouldConnect = messages && !messages.error
  useActionCable(shouldConnect ? {channel: 'MessagesChannel'} : null, {
    received({action, ...message}) {
      console.log({action, ...message})
      switch (action) {
        case 'create':
          return mutate('/social/messages', (mx = []) => [message, ...mx])
        case 'update':
          return mutate('/social/messages', (mx = []) => mx.map((m) => m.id == message.id ? message : m))
        case 'destroy':
          return mutate('/social/messages', (mx = []) => mx.filter((m) => m.id != message.id))
        default:
          console.log({action, message})
      }
    },
  })

  return messages
}

const Message = ({message, modal}) => pug`
  - const {user} = useAuth()
  - const fromMe = message.user_id == user.id

  .w-full.flex.px-2.pb-2(
    key=message.created_at
    className=(fromMe ? 'justify-end' : 'justify-start')
  )
    if fromMe
      div(className='w-1/2')

    .flex.flex-col.p-2.rounded-lg.border.shadow-lg(
      style={minWidth: '8rem'}
      className=(fromMe ? 'border-indigo-700 bg-indigo-600' : 'border-green-700 bg-green-600')
    )
      Link(to='/social/profiles/'+message.user_id)
        .text-xs.font-semibold(
          className=(fromMe ? 'text-indigo-200' : 'text-green-200')
        )= message.display_name

      .text-lg.text-white= message.content
      .w-full.flex.flex-row-reverse.text-indigo-200.text-xs
        if fromMe
          button.text-xs.text-indigo-200(onClick=()=>modal.open(message))
            Icon(icon='pen')
          .w-3

        = dayjs(message.created_at).fromNow()


    unless fromMe
      div(className='w-1/2')
`

export default () => pug`
  - const messages = useMessages()
  - const modal = useModal()

  for message in messages
    Message(key=message.id message=message modal=modal)

  EditMessage(modal=modal)
`
