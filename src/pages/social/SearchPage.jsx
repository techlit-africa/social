import fuzzysearch from 'fuzzysearch'
import dayjs from 'dayjs'

import Avatar from 'comps/core/Avatar'
import Suspend from 'comps/core/Suspend'
import Button from 'comps/core/Button'
import Input from 'comps/core/Input'
import useSwr from 'hooks/core/useSwr'
import useFocusOnMount from 'hooks/core/useFocusOnMount'

import Layout from 'comps/social/Layout'

const FILTER_INPUT_ID = 'filter-input'

export const useSearch = () => {
  const {data: profiles} = useSwr(`/social/profiles`)
  const {data: klasses} = useSwr(`/social/klasses`)

  const [klassId, setKlassId] = React.useState('')
  const [filter, setFilter] = React.useState('')

  useFocusOnMount(FILTER_INPUT_ID)

  const filteredProfiles = React.useMemo(() => {
    return profiles.filter((profile) => {
      if (klassId != '' && profile.klass_id != klassId) return false

      return filter == '' ||
        fuzzysearch(filter, profile.klass_name.toLowerCase()) ||
        fuzzysearch(filter, profile.real_name?.toLowerCase() || '') ||
        fuzzysearch(filter, profile.username) ||
        fuzzysearch(filter, profile.display_name.toLowerCase()) ||
        fuzzysearch(filter, profile.tagline.toLowerCase())
    })
  }, [profiles, klassId, filter])

  return {profiles, filteredProfiles, klassId, setKlassId, filter, setFilter, klasses}
}

const Ready = () => pug`
  - const {profiles, filteredProfiles, klassId, setKlassId, filter, setFilter, klasses} = useSearch()

  .flex-1.w-full.max-w-screen-sm.mx-auto.flex.flex-col.overflow-hidden
    .flex.flex-col.pt-4.pb-2
      .text-2xl.font-semibold.text-gray-700 #{filteredProfiles.length} / #{profiles.length} Profiles

      .grid.grid-cols-2.gap-x-2.pr-2
        select.px-2.py-1.border.border-gray-500.rounded(
          value=klassId
          onChange=(e)=>setKlassId(e.target.value)
        )
          option(value='') All Classes
          for klass in klasses
            option(key=klass.id value=klass.id)= klass.name

        input.px-2.py-1.border.border-gray-500.rounded(
          id=FILTER_INPUT_ID
          placeholder='Filter'
          value=filter
          onChange=(e)=>setFilter(e.target.value)
        )

    .flex-1.overflow-y-scroll#profileList
      .flex.flex-col.justify-end.pt-4
        for profile in filteredProfiles
          Link(
            to='/social/profiles/'+profile.id
            key=profile.id
          )
            button.text-left.mb-3.w-full.flex.flex-col.bg-gray-100.hover_bg-white.border.border-gray-500.rounded.shadow.hover_shadow-lg.transition
              // Header container
              .flex.items-start.justify-between
                // Profile content on the left
                .flex.p-2
                  // User avatar
                  Avatar(md profile=profile)

                  // A column of text
                  .flex.flex-col.pl-3.font-medium.text-gray-600.leading-tight
                    // Display name
                    .text-xl.text-black= profile.display_name
                    // Tagline
                    .text-lg.text-gray-800= profile.tagline
                    // Context
                    .flex.text-xs
                      .text-green-600.font-mono #{profile.klass_name.replaceAll(' ', '-')}/#{profile.username}
                      .pl-3 Logged in #{dayjs(profile.updated_at).fromNow()}.
`

export default () => pug`
  Layout(scratch): Ready
`
