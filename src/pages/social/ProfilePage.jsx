import Layout from 'comps/social/Layout'
import useAuth from 'hooks/core/useAuth'
import useSwr from 'hooks/core/useSwr'

import Header from 'pages/social/ProfilePage/Header'
import Storage from 'pages/social/ProfilePage/Storage'
import Projects from 'pages/social/ProfilePage/Projects'

const useProfile = () => {
  const {params} = Router.useRouteMatch("/social/profiles/:profileId")
  const {data: profile} = useSwr(`/social/profiles/${params.profileId}`)

  const {user} = useAuth()

  const mine = profile && profile.id == user.id

  return {...profile, mine}
}

const Ready = () => pug`
  - const profile = useProfile()

  if profile.error
    Redirect(to='/social/search')

  else
    Header(profile=profile)
    Storage(profile=profile)
    Projects(profile=profile)
`

export default () => pug`
  Layout: Ready
`
