import Layout from 'comps/social/Layout'
import Suspend from 'comps/core/Suspend'

import List from 'pages/social/MessagesPage/List'
import Form from 'pages/social/MessagesPage/Form'

export default () => pug`
  Layout(scratch)
    .flex-1.w-full.max-w-screen-sm.mx-auto.flex.flex-col.overflow-hidden
      .flex-1.overflow-y-scroll#messageList
        .flex.flex-col.justify-end.pt-4
          Suspend: List

      Form
`
