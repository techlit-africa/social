import dayjs from 'dayjs'

import Avatar from 'comps/core/Avatar'
import useSwr from 'hooks/core/useSwr'
import useLookup from 'hooks/core/useLookup'
import useModal from 'hooks/core/useModal'

import Layout from 'comps/social/Layout'
import ShowProject from 'modals/social/ShowProject'

export default () => pug`
  Layout(scratch)
    Ready
`

const useFeed = () => {
  const {data: projects} = useSwr('/social/projects')
  const {data: profiles} = useSwr('/social/profiles')

  const profilesById = useLookup(profiles, ({id}) => id)

  return {projects, profilesById}
}

const Ready = () => pug`
  - const {projects, profilesById} = useFeed()

  - const show = useModal()

  .flex-1.py-2.overflow-y-scroll
    for project in projects
      - const profile = profilesById[project.user_id]

      .my-4.w-full.max-w-sm.mx-auto.border.border-gray-500.rounded.shadow(key=project.id)
        Link(
          to='/social/profiles/'+profile.id
          key=profile.id
        )
          button.text-left.w-full.flex.flex-col
            // Header container
            .flex.items-start.justify-between
              // Profile content on the left
              .flex.p-2
                // User avatar
                Avatar(profile=profile)

                // A column of text
                .flex.flex-col.pl-3.font-medium.text-gray-600.leading-tight
                  // Display name
                  .text-black= profile.display_name
                  // Tagline
                  .text-xs.text-gray-800= profile.tagline
                  // Context
                  .flex(style={fontSize: '0.6em'})
                    .text-green-600.font-mono #{profile.klass_name.replaceAll(' ', '-')}/#{profile.username}
                    .pl-2 Logged in #{dayjs(profile.updated_at).fromNow()}.

        .flex.border-t.border-gray-400.leading-tight
          .flex-1.p-2
            .text-2xl.font-semibold.truncate= project.name
            .text-xs.text-gray-600 #{project.human_size} | #{dayjs(project.updated_at).fromNow()}
            .h-2
            .text-gray-800= project.description

          .p-2
            a.text-gray-500(href=project.download_path download)
              Icon(icon='ellipsis-v')

        case project.content_type.split('/')[0]
          when 'image'
            button.w-full.flex.flex-col.items-center.justify-center.bg-checkerboard-1.border-b.border-gray-400.transition.duration-200(
              style={height: '12em'}
              onClick=()=>show.open(project)
            )
              img.h-full.mx-auto(src=project.download_path)

          when 'audio'
            button.h-12.w-full.flex.flex-col.items-center.justify-center.hover_bg-gray-300.text-gray-700.border-b.border-gray-400.transition.duration-200
              audio.w-full(src=project.download_path controls)

          when 'video'
            button.w-full.flex.flex-col.items-center.justify-center.bg-black.text-gray-700.border-b.border-gray-400.transition.duration-200(style={height: '12em'})
              video.h-full.mx-auto(src=project.download_path controls)

          default
            button.w-full.flex.items-center.justify-center.bg-gray-300.border-b.border-gray-400(style={height: '12em'})
              Icon.text-3xl.text-gray-600(icon='file-upload')

  ShowProject(modal=show)
`
