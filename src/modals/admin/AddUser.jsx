import Modal from 'comps/core/Modal'

import Form from 'pages/admin/UsersPage/Form'

export default ({modal}) => pug`
  Modal(title='Add User' modal=modal)
    Form(onClose=modal.close)
`
