import Modal from 'comps/core/Modal'

import Form from 'pages/admin/KlassesPage/Form'

export default ({modal}) => pug`
  Modal(title='Add Class' modal=modal)
    Form(onClose=modal.close)
`
