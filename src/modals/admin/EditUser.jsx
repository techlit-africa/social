import {mutate} from 'swr'

import Modal from 'comps/core/Modal'
import useAuth from 'hooks/core/useAuth'

import Form from 'pages/admin/UsersPage/Form'

export default ({modal}) => pug`
  Modal(title='Edit User' modal=modal): Ready(modal=modal)
`

const useResetPassword = ({user, modal}) => {
  const {fetch} = useAuth()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      disabled: false,
      error: null,
    },
  )

  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    update({disabled: true})

    fetch(`/admin/users/${user.id}/reset_password`, {method: 'POST'})
      .then(({error}) => {
        setTimeout(() => {
          if (error) return setError(error)

          mutate('/admin/users', (users = []) => users.map((u) =>
            u.id == user.id
              ? {...u, password_digest: null}
              : u))

          setTimeout(modal.close)
        })
      })
      .catch(setError)
  }

  return {...state, submit}
}

const Ready = ({modal}) => pug`
  - const resetPassword = useResetPassword({user: modal.data, modal})

  .flex.flex-col.items-end
    if modal.data.password_digest
      button.text-blue-600(
        onClick=resetPassword.submit
        disabled=resetPassword.disabled
      ) Reset password as username
    else
      button.text-red-500(disabled) Password is username

    if resetPassword.error
      .text-red-500= resetPassword.error

  Form(user=modal.data onClose=modal.close)
`
