import {mutate} from 'swr'

import Modal from 'comps/core/Modal'
import useAuth from 'hooks/core/useAuth'

import Form from 'pages/admin/KlassesPage/Form'

const useDestroy = ({klass, modal}) => {
  const {fetch} = useAuth()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      disabled: false,
      error: null,
    },
  )

  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    update({disabled: true})

    fetch(`/admin/klasses/${klass.id}`, {method: 'DELETE'})
      .then(({error}) => {
        setTimeout(() => {
          if (error) return setError(error)

          mutate('/admin/klasses', (klasses = []) => klasses.filter((k) => k.id != klass.id))
          setTimeout(modal.close)
        }, 100)
      })
      .catch(setError)
  }

  return {...state, submit}
}

const Content = ({modal, klass}) => pug`
  - const destroy = useDestroy({klass, modal})

  .relative
    Form(klass=klass onClose=modal.close)

    if destroy.disabled
      .absolute.inset-0.flex.items-center.justify-center(style={backgroundColor: '#00000020'})
        Icon.text-4xl.animate-spin(icon='circle-notch')

  if destroy.error
    .text-red-500= destroy.error

  .h-3
  button.text-red-500.flex.items-center(
    onClick=destroy.submit
    disabled=destroy.disabled
  )
    Icon(icon='trash')
    .pl-3 Delete
`

export default ({modal}) => pug`
  Modal(title='Edit Class' modal=modal)
    Content(modal=modal klass=modal.data)
`
