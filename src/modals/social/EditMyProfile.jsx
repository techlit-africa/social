import {mutate} from 'swr'

import Button from 'comps/core/Button'
import InputField from 'comps/core/InputField'
import Modal from 'comps/core/Modal'
import useAuth from 'hooks/core/useAuth'
import useFocusOnMount from 'hooks/core/useFocusOnMount'

const DISPLAY_NAME_ID = 'display-name'
const TAGLINE_ID = 'tagline'

const useForm = (modal) => {
  useFocusOnMount(DISPLAY_NAME_ID)

  const {fetch, user, updateUser} = useAuth()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      disabled: false,
      error: null,

      klass: user.klass,
      username: user.username,
      display_name: user.display_name,
      tagline: user.tagline,
    },
  )

  const onChange = (propName) => (e) => update({
    [propName]: e.target.value,
    error: null,
  })

  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    update({disabled: true})

    const body = JSON.stringify({
      display_name: state.display_name,
      tagline: state.tagline,
    })

    fetch(`/social/profiles/${user.id}`, {method: 'PATCH', body})
      .then(({error, ...changes}) => {
        setTimeout(() => {
          if (error) return setError(error)

          mutate(`/social/profiles/${user.id}`)
          updateUser(changes)
          setTimeout(modal.close, 100)
        }, 1000)
      })
      .catch(setError)
  }

  return {
    ...state,
    update,
    onChange,
    submit,
  }
}

const Form = ({modal}) => pug`
  - const form = useForm(modal)

  .grid.grid-cols-3.gap-3
    InputField(
      wrapperClassName='col-span-1'
      disabled
      label='Class'
      defaultValue=form.klass
    )

    InputField(
      wrapperClassName='col-span-2'
      disabled
      label='Username'
      defaultValue=form.username
    )

    InputField(
      wrapperClassName='col-span-3'
      disabled=form.disabled
      id=DISPLAY_NAME_ID
      label='Display Name'
      value=form.display_name
      onChange=form.onChange('display_name')
    )

    InputField(
      wrapperClassName='col-span-3'
      disabled=form.disabled
      id=TAGLINE_ID
      label='Tagline'
      value=form.tagline
      onChange=form.onChange('tagline')
    )

    if form.error
      .col-span-3.text-red-500= form.error

    Button.col-span-3(
      disabled=form.disabled
      onClick=form.submit
    )
      if form.disabled
        Icon.animate-spin(icon='circle-notch')
      else
        | Save Changes
`

export default ({modal}) => pug`
  Modal(title='Edit Profile' modal=modal)
    Form(modal=modal)
`
