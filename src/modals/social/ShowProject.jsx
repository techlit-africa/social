import Modal from 'comps/core/Modal'

export default ({modal}) => pug`
  - const project = modal.data

  Modal(noPadding title=(project ? project.name : '...') modal=modal)
    if project
      .w-full.flex-1.items-center.justify-center.overflow-hidden.rounded-b.bg-checkerboard-2
        case project.content_type.split('/')[0]
          when 'image'
            img.mx-auto(src=project.download_path style={maxHeight: '90vh'})
          when 'audio'
            audio.w-full(src=project.download_path controls)
          when 'video'
            video.mx-auto(src=project.download_path controls style={maxHeight: '90vh'})
          default
            Icon.text-4xl(icon='file-upload')
`
