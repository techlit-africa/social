import {mutate} from 'swr'

import Button from 'comps/core/Button'
import InputField from 'comps/core/InputField'
import Modal from 'comps/core/Modal'
import TextAreaField from 'comps/core/TextAreaField'
import useAuth from 'hooks/core/useAuth'
import useFocusOnMount from 'hooks/core/useFocusOnMount'

const FILE_INPUT_ID = 'project-file'
const NAME_INPUT_ID = 'project-name'
const DESCRIPTION_INPUT_ID = 'project-description'

const FILE_ICONS = {
  image: 'file-image',
  audio: 'file-audio',
  video: 'file-video',
}

const fileIcon = (type) =>
  FILE_ICONS[type.split('/')?.[0]] || 'file-upload'

const useForm = (modal) => {
  useFocusOnMount(NAME_INPUT_ID)

  const {fetch, user, token} = useAuth()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      disabled: false,
      dropping: false,
      error: null,

      kind: modal.data.kind,
      name: modal.data.name,
      description: modal.data.description,
      file: null,

      originalFilename: modal.data.filename,
      originalType: modal.data.content_type,

      xhr: null,
    },
  )

  const onChange = (propName) => (e) => update({
    [propName]: e.target.value,
    error: null,
  })

  const onDragEnter = (e) => {
    e.preventDefault()
    e.stopPropagation()
    update({dragging: true})
  }
  const onDragOver = (e) => {
    e.preventDefault()
    update({dragging: true})
  }
  const onDragLeave = (e) => {
    e.preventDefault()
    update({dragging: false})
  }
  const onClickFile = () => document.getElementById(FILE_INPUT_ID)?.click()

  const onDropFile = (e) => {
    onDragLeave(e)
    const {files} = e.dataTransfer
    update({file: files[0]})
  }
  const onChangeFile = (e) => update({file: e.target.files[0]})

  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    update({disabled: true})

    const data = new FormData()
    data.append('kind', state.kind)
    data.append('name', state.name)
    data.append('description', state.description)
    if (state.file) data.append('file', state.file)

    const xhr = new XMLHttpRequest()
    xhr.open('PATCH', `/api/social/projects/${modal.data.id}`)
    xhr.setRequestHeader('Authorization', `Bearer ${token}`)
    xhr.send(data)
    update({xhr})

    xhr.onload = (e) => {
      if (xhr.readyState != 4) return

      try {
        const project = JSON.parse(xhr.response)

        if (project?.error || xhr.status > 299) {
          setError(project?.error || "Something went wrong")
        } else {
          mutate(`/social/profiles/${user.id}`)
          setTimeout(modal.close, 100)
        }
      } catch (error) {
        setError(error)
      }
    }

    xhr.onerror = (e) => {
      setError(xhr.statusText)
      xhr.send(null)
    }
  }

  const abort = () => xhr.abort()

  return {
    ...state,
    update,
    onChange,
    onDragEnter,
    onDragOver,
    onDragLeave,
    onClickFile,
    onChangeFile,
    onDropFile,
    submit,
    abort,
  }
}

const useDestroy = ({project, modal}) => {
  const {fetch} = useAuth()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      disabled: false,
      error: null,
    },
  )

  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    update({disabled: true})

    fetch(`/social/projects/${project.id}`, {method: 'DELETE'})
      .then(({error}) => {
        setTimeout(() => {
          if (error) return setError(error)

          mutate(`/social/profiles/${project.user_id}`)
          setTimeout(modal.close)
        }, 1000)
      })
      .catch(setError)
  }

  return {...state, submit}
}

const Form = ({modal}) => pug`
  - const form = useForm(modal)
  - const destroy = useDestroy({project: modal.data, modal})

  .relative
    .grid.grid-cols-6.gap-3
      .col-span-1
      .col-span-4.flex-col
        label.text-gray-600 Project File
        .h-2
        input.hidden(
          type='file'
          disabled=form.disabled
          id=FILE_INPUT_ID
          onChange=form.onChangeFile
        )
        button.w-full.h-32.flex.flex-col.items-center.justify-center.hover_text-gray-700.hover_bg-gray-300.border.rounded.shadow.hover_shadow-lg(
          className=(form.dragging ? 'text-blue-600 bg-blue-200 border-blue-400' : 'text-gray-600 bg-gray-200 border-gray-400')
          onClick=form.onClickFile
          onDragEnter=form.onDragEnter
          onDragOver=form.onDragOver
          onDragLeave=form.onDragLeave
          onDrop=form.onDropFile
        )
          Icon.text-4xl(icon=fileIcon(form.file && form.file.type || form.originalType))
          .max-w-sm.truncate= form.file && form.file.name || form.originalFilename
      .col-span-1

      InputField(
        wrapperClassName='col-span-4'
        disabled=form.disabled
        id=NAME_INPUT_ID
        label='Name'
        value=form.name
        onChange=form.onChange('name')
      )

      InputField(
        wrapperClassName='col-span-2'
        disabled
        label='Kind'
        defaultValue=form.kind
      )

      TextAreaField.resize-none(
        wrapperClassName='col-span-6'
        rows=2
        disabled=form.disabled
        id=DESCRIPTION_INPUT_ID
        label='Description'
        value=form.description
        onChange=form.onChange('description')
      )

      if form.error
        .col-span-6.text-red-500= form.error

      Button.col-span-6(
        disabled=form.disabled
        onClick=form.submit
      )
        if form.disabled
          Icon.animate-spin(icon='circle-notch')
        else
          | Save Project

  if destroy.disabled
    .absolute.inset-0.flex.items-center.justify-center(style={backgroundColor: '#00000020'})
      Icon.text-4xl.animate-spin(icon='circle-notch')

  if destroy.error
    .text-red-500= destroy.error

  .h-3

  button.text-red-500.flex.items-center(
    onClick=destroy.submit
    disabled=destroy.disabled
  )
    Icon(icon='trash')
    .pl-2 Delete this project
`

export default ({modal}) => pug`
  Modal(title='Update Project' modal=modal)
    Form(modal=modal)
`
