import Modal from 'comps/core/Modal'
import useAuth from 'hooks/core/useAuth'

import Form from 'pages/social/MessagesPage/Form'

const useDestroy = ({message, modal}) => {
  const {fetch} = useAuth()

  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      disabled: false,
      error: null,
    },
  )

  const setError = (error) => update({disabled: false, error})

  const submit = () => {
    update({disabled: true})

    fetch(`/social/messages/${message.id}`, {method: 'DELETE'})
      .then(({error}) => {
        setTimeout(() => {
          if (error) return setError(error)

          setTimeout(modal.close)
        }, 100)
      })
      .catch(setError)
  }

  return {
    ...state,
    submit,
  }
}

const Content = ({modal, message}) => pug`
  - const destroy = useDestroy({message, modal})

  .relative
    Form(message=message onClose=modal.close)

    if destroy.disabled
      .absolute.inset-0.flex.items-center.justify-center(style={backgroundColor: '#00000020'})
        Icon.text-4xl.animate-spin(icon='circle-notch')

  if destroy.error
    .text-red-500= destroy.error

  .h-3
  button.text-red-500.flex.items-center(
    onClick=destroy.submit
    disabled=destroy.disabled
  )
    Icon(icon='trash')
    .pl-3 Delete
`

export default ({modal}) => pug`
  Modal(title='Edit Message' modal=modal)
    Content(modal=modal message=modal.data)
`
