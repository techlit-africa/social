import Layout from 'comps/typing/Layout'

export default ({children}) => pug`
  Layout
    .w-full.flex-1.flex.flex-col.items-center.justify-center.p-8
      .w-full.max-w-screen-md.border.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow.rounded
        .grid.grid-cols-3
          Tab.border-r(path='/typing/lessons' icon='keyboard') Lessons
          Tab.border-r(path='/typing/stats' icon='chart-bar') Stats
          Tab(path='/typing/tips' icon='hand-sparkles') Tips
        .p-4= children
`

const doesMatchPath = (path) => location.pathname == path

const Tab = ({path, icon, className, children}) => pug`
  - const history = Router.useHistory()

  button.p-4.text-2xl.text-blue-500.dark_text-blue-400.border-gray-500(
    className=className+' '+(doesMatchPath(path) ? '' : 'border-b bg-gray-200 dark_bg-black hover_text-blue-600 dark_hover_text-blue-500')
    onClick=()=>history.push(path)
  )
    Icon(icon=icon)
    span.pl-3= children
`
