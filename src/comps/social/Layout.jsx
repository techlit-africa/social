import Suspend from 'comps/core/Suspend'
import useAuth from 'hooks/core/useAuth'

const Tab = ({path, icon, title, children}) => pug`
  - const match = Router.useRouteMatch({path})

  Link.h-full.flex.items-center(
    className=(match ? 'border-b-2 border-blue-600' : '')
    to=path
  )
    button.mx-1.py-1.px-3.rounded.hover_bg-gray-300.transition(
      className=(match ? 'text-blue-600' : 'text-gray-700')
    )
      Icon.text-2xl(icon=icon)
      .hidden.sm_inline.pl-2= title
`

export default ({scratch = false, small = false, children}) => pug`
  - const {user} = useAuth()

  // Full-page container
  .w-full.h-screen.flex.flex-col.bg-gray-200.dark_bg-gray-800
    // Navbar container
    .w-full.h-12.flex.items-center.justify-center.border-b.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow
      // Centered links
      .h-full.flex.items-center
        Tab(icon='users'       title='Feed'       path='/social/feed')
        Tab(icon='comments'    title='Chat'       path='/social/messages')
        Tab(icon='search'      title='Search'     path='/social/search')
        Tab(icon='user-circle' title='My Profile' path='/social/profiles/'+user.id)

        if user.klass_name == 'teachers'
          Tab(icon='cog' title='Admin' path='/admin')

    if scratch
      Suspend= children

    else
      // App content container (uses remaining height, scrolls vertically)
      .flex-1.h-full.w-full.overflow-y-scroll.p-2(
        className=(small ? 'pt-16' : 'pt-8')
      )
        // Page card
        .w-full.mx-auto.border.border-gray-500.bg-gray-100.shadow.rounded.p-2(
          className=(small ? 'max-w-lg' : 'max-w-screen-sm')
        )
          // Suspended page content
          Suspend= children
`
