export default ({id, className, children, ...props}) => pug`
  .h-2
  button.py-2.px-4.text-2xl.bg-blue-500.hover_bg-blue-600.disabled_bg-gray-500.text-white.border.border-blue-600.rounded.shadow.hover_shadow-lg(
    id=id className=className
    ...props
  )= children
`
