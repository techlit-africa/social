import TextArea from 'comps/core/TextArea'

export default ({id, className, wrapperClassName, label, ...props}) => pug`
  // Label & input container
  .w-full.h-full.flex.flex-col(className=wrapperClassName)

    // Label & spacer if label given
    if label
      label.text-gray-600= label
      .h-2

    // Input (props passed here)
    TextArea(id=id className=className ...props)
`
