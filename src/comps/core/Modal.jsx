import useKeysDown from 'hooks/core/useKeysDown'

const useCloseOnEsc = (modal) => {
  const onEsc = React.useRef()
  onEsc.current = (e) => e.code == 'Escape' && modal.close()
  useKeysDown({onKeyDownRef: onEsc})
}

export default ({noPadding, modal, title = '', children}) => pug`
  - useCloseOnEsc(modal)

  if modal.isOpen
    .fixed.inset-0.overflow-y-scroll.z-50
      .min-h-screen.w-full.flex.flex-col.items-center.justify-center.p-2(
        style={backgroundColor: '#000000AA'}
        onClick=modal.close
      )
        .flex.flex-col.w-full.bg-gray-100.shadow-lg.rounded(
          style={maxWidth: '40em'}
          onClick=(e)=>{e.stopPropagation()}
        )
          if title
            .w-full.h-10.flex.justify-between.border-b.border-gray-500.bg-gray-100.shadow.rounded-t
              .h-full.flex.items-center.text-xl.font-medium.text-gray-900.pl-2
                = title

              unless modal.locked
                button.h-full.flex.items-center.border-l.border-gray-300.hover_bg-red-500.text-red-500.hover_text-red-100.px-3.rounded-tr(onClick=modal.close)
                  Icon(icon='times')

          .w-full(
            className=(noPadding ? '' : 'p-4')
          )= children
`
