export default ({id, className, label, ...props}) => pug`
  textarea.px-2.py-1.text-2xl.bg-white.border.border-gray-500.rounded(
    id=id className=className
    ...props
  )
`
