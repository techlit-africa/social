import runes from 'runes'

const CLASS_NAMES = {
  sm: "h-10 w-10 text-xl",
  md: "h-16 w-16 text-3xl",
  lg: "h-24 w-24 text-5xl",
}

export default ({lg, md, sm, profile}) => pug`
  .rounded-full.flex-shrink-0.flex.items-center.justify-center.font-bold.bg-gray-600.text-white(
    className=CLASS_NAMES[lg && "lg" || md && "md" || "sm"]
  )= runes.substr(profile.display_name, 0, 1)
`
