import Input from 'comps/core/Input'

export default ({id, className, wrapperClassName, label, ...props}) => pug`
  // Label & input container
  .w-full.h-full.flex.flex-col(className=wrapperClassName)

    // Label & spacer if label given
    if label
      label.text-gray-600= label
      .h-2

    // Input (props passed here)
    Input(id=id className=className ...props)
`
