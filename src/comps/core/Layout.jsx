import Suspend from 'comps/core/Suspend'

export default ({children}) => pug`
  .w-full.h-screen.bg-gray-200.dark_bg-gray-800.px-2
    img.w-64.mx-auto.py-10(src='logo.png')
    Suspend= children
`
