export default ({children}) => pug`
  ErrorBoundary(FallbackComponent=Failed)
    React.Suspense(fallback=${pug`Loading`})
      = children
`

export const Loading = () => pug`
  .w-full.flex.items-center.justify-center.p-12
    Icon.animate.animate-spin.text-gray-800.dark_text-white.text-5xl(icon='circle-notch')
`

export const Failed = () => pug`
  .w-full.flex.items-center.justify-center.p-12
    Icon.text-red-500.dark_text-white.text-5xl(icon='exclamation-triangle')
`
