import Select from 'comps/core/Select'

export default ({id, className, wrapperClassName, label, children, ...props}) => pug`
  // Label & input container
  .w-full.h-full.flex.flex-col(className=wrapperClassName)

    // Label & spacer if label given
    if label
      label.text-gray-600= label
      .h-2

    // Select (props passed here)
    Select(id=id className=className ...props)
      = children
`
