import Suspend from 'comps/core/Suspend'

import Layout from 'comps/social/Layout'

const Tab = ({path, icon, title, children}) => pug`
  - const match = Router.useRouteMatch({path})

  Link.h-full.flex.items-center(
    className=(match ? 'border-b-2 border-blue-600' : '')
    to=path
  )
    button.mx-1.py-1.px-3.rounded.hover_bg-gray-300.transition(
      className=(match ? 'text-blue-600' : 'text-gray-700')
    )
      Icon.text-2xl(icon=icon)
      .hidden.sm_inline.pl-2= title
`


export default ({children}) => pug`
  Layout(scratch)
    // App content container (uses remaining height, scrolls vertically)
    .flex-1.h-full.w-full.overflow-y-scroll.p-2.pt-8
      // Page card
      .w-full.max-w-lg.mx-auto.border.border-gray-500.bg-gray-100.shadow.rounded
        .h-12.flex.items-ceter.border-b.border-gray-500.px-2
          Tab(icon='tag'          title='Classes' path='/admin/classes')
          Tab(icon='address-card' title='Users'   path='/admin/users')
          Tab(icon='sitemap'      title='Control' path='/admin/control')

        .p-2
          Suspend= children
`
