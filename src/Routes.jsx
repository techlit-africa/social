import Helmet from 'react-helmet'

import useAuth from 'hooks/core/useAuth'

import LoginPage from 'pages/core/LoginPage'
import ChangePasswordPage from 'pages/core/ChangePasswordPage'

import MessagesPage from 'pages/social/MessagesPage'
import FeedPage from 'pages/social/FeedPage'
import SearchPage from 'pages/social/SearchPage'
import ProfilePage from 'pages/social/ProfilePage'

import LessonsPage from 'pages/typing/LessonsPage'
import StatsPage from 'pages/typing/StatsPage'
import TipsPage from 'pages/typing/TipsPage'
import ExercisePage from 'pages/typing/ExercisePage'
import SubmissionPage from 'pages/typing/SubmissionPage'
import TestSubmissionPage from 'pages/typing/TestSubmissionPage'
import TestPage from 'pages/typing/TestPage'

import KlassesPage from 'pages/admin/KlassesPage'
import UsersPage from 'pages/admin/UsersPage'
import ControlPage from 'pages/admin/ControlPage'

export default () => pug`
  - const {user, redirect} = useAuth()

  Switch
    Route(path='/login')
      Helmet: title Login
      LoginPage

    Route(path='/typing')
      Helmet: title Typing

      unless user
        Redirect(to='/login?redirect=/typing')

      else
        Switch
          Route(path='/typing/lessons'): LessonsPage
          Route(path='/typing/stats'): StatsPage
          Route(path='/typing/tips'): TipsPage
          Route(path='/typing/exercises/:id'): ExercisePage
          Route(path='/typing/submissions/:id'): SubmissionPage
          Route(path='/typing/test-submissions/:id'): TestSubmissionPage
          Route(path='/typing/test'): TestPage
          Route: Redirect(to='/typing/lessons')

    Route
      unless user
        Redirect(to='/login')

      else
        Switch
          Route(path='/change-password'): ChangePasswordPage

          Route(path='/social')
            Helmet: title Social

            Switch
              Route(path='/social/profiles/:id'): ProfilePage
              Route(path='/social/feed'): FeedPage
              Route(path='/social/search'): SearchPage
              Route(path='/social/messages'): MessagesPage
              Route: Redirect(to='/social/feed')

          Route(path='/admin')
            unless user.klass_name == 'teachers'
              Redirect(to='/social/feed')

            else
              Helmet: title Admin

              Switch
                Route(path='/admin/classes'): KlassesPage
                Route(path='/admin/users'): UsersPage
                Route(path='/admin/control'): ControlPage
                Route: Redirect(to='/admin/control')

          Route: Redirect(to=(redirect || '/social/feed'))
`
