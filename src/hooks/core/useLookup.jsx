export default (sx = [], f = () => {}) =>
  React.useMemo(() => {
    const lookup = {}

    sx.forEach((x) => {
      const key = f(x)
      lookup[key] = x
    })

    return lookup
  }, [sx])
