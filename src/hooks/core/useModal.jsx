export default (defaultData) => {
  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      data: defaultData,
      isOpen: false,
      isLocked: false,
    }
  )

  return {
    ...state,

    open: (data = defaultData) => state.isLocked || update({data, isOpen: true}),
    close: () => state.isLocked || update({data: defaultData, isOpen: false}),

    lock: () => update({isLocked: true}),
    unlock: () => update({isLocked: false}),
  }
}
