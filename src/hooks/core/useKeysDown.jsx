export default ({onKeyDownRef, onKeyUpRef} = {}) => {
  const [shift, setShift] = React.useState(false)
  const [capsLock, setCapsLock] = React.useState(false)

  const [keysDown, setKeyDown] = React.useReducer((keys, newKeys) => ({...keys, ...newKeys}), {})
  React.useEffect(() => {
    const syncKeyFn = (isDown) => (event) => {
      isDown ? onKeyDownRef?.current?.(event) : onKeyUpRef?.current?.(event)

      setShift(event.shiftKey)
      setCapsLock(event.getModifierState('CapsLock'))
      setKeyDown({[event.code]: isDown})
    }

    const _onKeyDown = syncKeyFn(true)
    const _onKeyUp = syncKeyFn(false)

    window.addEventListener('keydown', _onKeyDown)
    window.addEventListener('keyup', _onKeyUp)

    return () => {
      window.removeEventListener('keydown', _onKeyDown)
      window.removeEventListener('keyup', _onKeyUp)
    }
  }, [])

  return [shift, capsLock, keysDown]
}
