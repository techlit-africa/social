import {ActionCableProvider as OriginalProvider, useActionCable} from 'use-action-cable'

import useAuth from 'hooks/core/useAuth'

const URL_BASE = NODE_ENV == 'development' ? 'ws://localhost:4000/cable' : '/cable'

const useUrl = () => {
  const {token} = useAuth()
  return token ? `${URL_BASE}?token=${token}` : null
}

export const ActionCableProvider = ({children}) => pug`
  - const url = useUrl()
  if url
    OriginalProvider(url=url)= children
  else
    = children
`

export default useActionCable
