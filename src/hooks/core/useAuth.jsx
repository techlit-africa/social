import createHook from 'lib/core/createHook'

const getUserFromStorage = () => {
  try { return JSON.parse(localStorage.getItem('techlit.qwerty.user')) }
  catch (e) { return null }
}

const setUserInStorage = (user = null) => {
  localStorage.setItem('techlit.qwerty.user', JSON.stringify(user))
}

const getRedirectFromSearch = () =>
  new URLSearchParams(window.location.search).get('redirect')

export const {Provider: AuthProvider, useContext: useAuth} = createHook(() => {
  // Get user from storage if it's there
  const [state, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      user: getUserFromStorage(),
      redirect: getRedirectFromSearch(),
    },
  )

  React.useEffect(() => update({redirect: getRedirectFromSearch()}), [window.location.search])

  // Save user in storage so that, when returning to the page, you're still logged in
  React.useEffect(() => setUserInStorage(state.user), [state.user])

  // Change some attributes on the user
  const updateUser = (changes) => update({user: {...state.user, ...changes}})

  // Logging out is just throwing away the user/token & setting an optional redirect
  const logout = (redirect = null) => update({user: null, redirect})

  // All API calls must be authenticated with the token gotten from the user
  // This wrapper also handles json deserialization
  const fetch = React.useCallback(async (path, ops = {}) => {
    const token = state.user?.token

    const rep = await window.fetch(`/api${path}`, {
      ...ops,
      headers: {
        'Content-Type': 'application/json',
        ...(token ? {'Authorization': `Bearer ${token}`} : null),
        ...(ops.headers || {}),
      },
    })
    if (rep.status == 401) return logout()
    return await rep.json()
  }, [state.user?.token])

  // Logging in handles the API call & regular errors
  const login = React.useCallback(async (username, password) => {
    const body = JSON.stringify({username, password})

    const {error, ...user} = await fetch('/sessions', {method: 'POST', body})
    if (error) throw error
    if (!user?.token) throw 'Login failed'

    update({user})
  }, [fetch])

  return {...state, fetch, updateUser, token: state.user?.token, login, logout}
})

export default useAuth
