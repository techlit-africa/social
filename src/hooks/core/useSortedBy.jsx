export default (xs = [], f = () => {}) =>
  React.useMemo(() => xs.sort(f), [xs])
