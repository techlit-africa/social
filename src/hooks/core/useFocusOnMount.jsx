export default (id) => {
  const focus = () => document.getElementById(id)?.focus()

  React.useEffect(() => {
    setTimeout(() => {
      focus()
    }, 100)
  }, [])

  return focus
}
