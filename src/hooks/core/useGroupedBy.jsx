export default (xs = [], f = () => {}) =>
  React.useMemo(() => {
    const grouped = {}

    xs.forEach((x) => {
      const key = f(x)
      if (!grouped[key]) grouped[key] = []
      grouped[key].push(x)
    })

    return grouped
  }, [xs])
