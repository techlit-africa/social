import useSWR, {SWRConfig} from 'swr'

import useAuth from 'hooks/core/useAuth'

export const SWRProvider = ({children}) => {
  const {fetch} = useAuth()

  const swrConfig = {
    fetcher: fetch,
    suspense: true,
    revalidateOnMount: false,
    dedupingInterval: 100,
    focusThrottleInterval: 10000,
    refreshInterval: 30000,
  }

  return pug`
    SWRConfig(value=swrConfig) #{children}
  `
}

export default useSWR
