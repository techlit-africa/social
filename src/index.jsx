import {render} from 'react-dom'

import {AuthProvider} from 'hooks/core/useAuth'
import {ActionCableProvider} from 'hooks/core/useActionCable'
import {SWRProvider} from 'hooks/core/useSwr'

import {SoundsProvider} from 'hooks/typing/useSounds'

import Routes from 'Routes'

console.log('booted')

import 'init/core/fontawesome'
import 'init/core/dayjs'

import 'styles/index.css'

const Main = () => pug`
  React.StrictMode
    Router.BrowserRouter
      SoundsProvider
        AuthProvider
          SWRProvider
            ActionCableProvider
              Routes
`

render(pug`Main`, document.getElementById('root'))

if (NODE_ENV == 'development') module.hot?.accept?.()
