require 'active_support/core_ext/integer/time'

Rails.application.configure do
  config.cache_classes = true
  config.eager_load = true
  config.consider_all_requests_local = false
  config.public_file_server.enabled = true
  config.active_storage.service = :local

  config.active_support.report_deprecations = false

  config.active_record.dump_schema_after_migration = false

  config.log_level = :info
  config.log_tags = %i[request_id]
  config.log_formatter = ::Logger::Formatter.new
  logger           = ActiveSupport::Logger.new(STDOUT)
  logger.formatter = config.log_formatter
  config.logger    = ActiveSupport::TaggedLogging.new(logger)

  config.project_storage_root = ENV.fetch('SOCIAL_STORAGE_DIRECTORY', '/data/http/users')
end
