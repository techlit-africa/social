Rails.application.routes.draw do
  mount ActionCable.server => '/cable'

  get '/download/users/:username/:name', to: 'social/projects#download'

  scope '/api' do
    resources :sessions, only: %i[create]

    namespace :social do
      resources :klasses, only: %i[index]
      resources :profiles, except: %i[new edit create]
      resources :messages, except: %i[new edit]
      resources :projects, except: %i[new edit]
    end

    namespace :typing do
      resources :stats, only: %i[index]
      resources :lessons, only: %i[index]
      resources :exercises, only: %i[show]
      resources :submissions, only: %i[create show]
      resources :test_submissions, only: %i[create show]
    end

    namespace :admin do
      resources :klasses, except: %i[new edit]
      resources :users, except: %i[new edit destroy] do
        member do
          post :reset_password
        end
      end

      resources :control do
        collection do
          post :logout
          post :reboot
          post :shutdown
        end
      end
    end

    match '*path', to: 'application#not_found', via: %i[get post put patch delete]
  end

  get '*path', to: 'application#client_html', constraints: -> (req) do
    !req.xhr? && req.format.html?
  end
end
