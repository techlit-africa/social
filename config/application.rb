require_relative 'boot'

require 'rails'
require 'active_model/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_cable/engine'

Bundler.require(*Rails.groups)

require_relative '../lib/download_proxy'

module Social
  class Application < Rails::Application
    config.load_defaults 7.0

    config.time_zone = 'Nairobi'
    config.eager_load_paths << Rails.root.join('lib')

    config.api_only = true

    config.project_storage_per_user = ENV.fetch('SOCIAL_STORAGE_LIMIT', '536870912').to_i

    config.middleware.use DownloadProxy
  end
end
