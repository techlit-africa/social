rails_env = ENV.fetch('RAILS_ENV', 'development')

threads 5, 5

worker_timeout 3600 if rails_env == 'development'
port ENV.fetch('PORT', 3000)
environment rails_env
pidfile ENV.fetch('PIDFILE', 'tmp/pids/server.pid')

plugin :tmp_restart
